                                                         % if you need a4paper
\documentclass[a4paper, 10pt, conference]{ieeeconf}      % Use this line for a4
                                                          % paper

\IEEEoverridecommandlockouts                              % This command is only
                                                          % needed if you want to
                                                          % use the \thanks command
\overrideIEEEmargins
% See the \addtolength command later in the file to balance the column lengths
% on the last page of the document



% The following packages can be found on http:\\www.ctan.org
\usepackage{graphicx} % for pdf, bitmapped graphics files
\usepackage{pgfplots}
%\usepackage{epsfig} % for postscript graphics files
%\usepackage{mathptmx} % assumes new font selection scheme installed
%\usepackage{times} % assumes new font selection scheme installed
\usepackage{amsmath} % assumes amsmath packge installed
%\usepackage{amssymb}  % assumes amsmath package installed

\title{\LARGE \bf
  Realtime Visual-Inertial Pose Estimation on a Mobile Phone
}

%\author{ \parbox{3 in}{\centering Huibert Kwakernaak*
%         \thanks{*Use the $\backslash$thanks command to put information here}\\
%         Faculty of Electrical Engineering, Mathematics and Computer Science\\
%         University of Twente\\
%         7500 AE Enschede, The Netherlands\\
%         {\tt\small h.kwakernaak@autsubmit.com}}
%         \hspace*{ 0.5 in}
%         \parbox{3 in}{ \centering Pradeep Misra**
%         \thanks{**The footnote marks may be inserted manually}\\
%        Department of Electrical Engineering \\
%         Wright State University\\
%         Dayton, OH 45435, USA\\
%         {\tt\small pmisra@cs.wright.edu}}
%}

\author{Federico Camposeco Paulsen% <-this % stops a space
}


\begin{document}



\maketitle
\thispagestyle{empty}
\pagestyle{empty}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{abstract}
Recent inclusions of inertial sensors, cameras and high-speed processors in smartphones are introducing new possibilities for interaction and generation of media content. This is an exciting moment in the fields of Computer Vision and Sensor Fusion, as this new features enable a whole new range of applications that call for new solutions which specifically target mobile hardware. 

In this paper, a new approach to the mobile integration of visual and inertial data is proposed. The pipeline aims to provide a metric pose estimation in realtime that can serve as infrastructure for other applications, such as dense live reconstruction as described in \cite{Tankanen_3dlive_2013}. The methods developed are all submitted to the constrains on mobile phones and are intended to tackle the specific challenges of such platform under hand-held motion. The resulting system is experimentally tested and proves to overcome the limitations of current mobile devices, while successfully providing an accurate estimation of the metric scale and pose of the phone with respect to an inertial frame. 

\end{abstract}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{INTRODUCTION}

In biological vision, Structure from Motion (SfM) refers to the phenomenon by which humans can extract 3D information from 2D retinal {\em images} of a moving scene. In Computer Vision, SfM tries to emulate such phenomenon using a single camera in motion. However, while humans are equipped with proprioception that provide information about the movement of the eyes with respect to the world, a digital camera does not \cite{corke_introduction_2007}. A natural approach should therefore be the inclusion of inertial measurement units (IMUs) that can act as such proprioceptors in humans. 

It is an advantage the recent inclusion of such IMU's in current smartphones, as the input from such devices can be readily used for integration with visual input to estimate pose, which in turn would enable countless applications such as Augmented Reality or 3D Scanning capabilities. While in theory both, the camera and IMU, are able to obtain the pose of the moving phone, an integration of acceleration over time yields position in meters, while a monocular position estimation would provide position up to scale \cite{Hartley2000}. 

Here a new method for integration of visual and intertial data is proposed. The scheme is designed to tackle the shortcomings of the inertial sensors available in the used mobile device, as well as on the complexity of the targeted hand-held motion and the visual scale ambiguity. In benefit of a more flexible system, the visual and inertial inputs are fused at a high-level, treating each estimation as separate pipeline. For the implementation, the visual estimation is provided using a modified version of Klein and Murray's PTAM \cite{klein2007parallel}. The novelty of this paper is the {\em robust}, efficient and automatic scale estimation targeted specifically at hand-held motion. 

\section{BACKGROUND}

\subsection{Structure from Motion}
Addressing the computational complexity issues and with emphasis on on-line monocular solutions to the SfM problem (also referred-to as monocular SLAM), new methods have appeared in the recent years. These new methods address the issue using different approaches and can be grouped into two main approaches \cite{strasdat_real-time_2010}. 

\begin{itemize} 
  \item Filtering Methods: They marginalise out past poses (Markovian assumption) and intend to summarize the information gained over time with a probability distribution in a single filter. \textit{E.g.} \cite{davison_monoslam:_2007}. 
  \item Keyframe Methods: They retain the non-linear optimisation approach (Bundle Adjustment), but they heuristically select a sub-sample of past frames (dubbed Keyframes) to process in the optimization step.  \textit{E.g.} \cite{klein2007parallel}.
\end{itemize}
Due to the efficiency inherent to Klein and Murray's method \cite{klein2007parallel}, it is used as a basis for the visual pose estimation. A brief notion of the system is given in the sequel.
  \subsubsection{Parallel Tracking and Mapping}
 In this system, the task of performing realtime SLAM/SfM is divided into two separate and parallel processes, or in computer terms, threads. The method can be summarized into the following main aspects:
 \begin{itemize}
   \item Tracking and mapping processes are separated and run in parallel.
   \item The mapping process will rely on keyframe selection to perform bundle adjustment on them. 
   \item Tracking process will rely on the actual map to localize the camera at each time step. Ergo, the map must be first initialized somehow in order for the tracking to begin.
   \item Map get initialized by a stereo pair (usual epipolar pipeline see \cite{Hartley2000}).
   \item Map can grow efficiently by epipolar search. 
 \end{itemize}
 Such scheme allows for the mapping side to not be disturbed or modified at all whenever a new frame arrives, and only process new data in the form of {\em keyframes}, as opposed to the filtering methods that modify all the landmarks at each frame (at rates of at least 20ms between frames). The efficiency and realtime capabilities of the system come from not only reducing the computational complexity of the SLAM/SfM problem, but relegating the remaining costly functions to a thread separate from the tracking thread. 

\subsection{Inertial Navigation Systems}
The mobile platform used in this research is a Samsung Galaxy S3, which has an integrated gyroscope-accelerometer monolithic Micor Electro-Mechanical (MEMS) device; the LSM330DLC from STMicroelectronics. It features a 3D accelerometer/gyro module that has the advantage of having collinear axes for both sensors, such that no gyroscope-to-accelerometer frame of reference transformation needs to be estimated without a significant effect on accuracy. However, noise and bias are very significant, so they will need to be calibrated and, in particular, the bias will need to be estimated to account for bias drift over time and its uncalibrated temperature dependency. 

\subsubsection{Gyroscope}
Two main error characteristics of gyroscopes, as in \cite{woodman_introduction_2007}, are static bias error and angle random walk error. \textbf{Static bias} is the output of the gyroscope when the device is set at rest; the offset of the output from the true value and it is measured in angle units over time units. This can lead to  simple dimensional analysis to find that this error, $\epsilon_b$ causes an error linear with time with respect to position:
    \begin{align}
      \theta(t) = \epsilon_b \cdot t
    \end{align}
    This bias can be easily determined by measuring and averaging a relatively long period of time when the device is not under rotation. Although static bias amounts for the majority of the error, this bias does have a dependency with time because of temperature dependencies, this effect is damped by the proposed algorithm as in Section \ref{}. 
   
    \textbf{Angle random walk} comes from thermo-mechanical events that operate at much higher rate than the sampling frequency. It can be easily modeled as additive white noise. The effect of this noise, $\epsilon_g$, on the resulting orientation estimation from simple rectangle-rule integration over time $t = n\delta t$ for $n$ samples is:
  \begin{align}
    \int_0^t \epsilon_g(\tau)\, \mathrm{d}\tau = \delta t \sum_i^n N_i \label{eq:integ_noise}
  \end{align}
With $N_i$ samples from the random uncorrelated process. Using standard formulations we can take the first and second moment of Eq. \ref{eq:integ_noise} we obtain the evolution of variance and mean of the error of the integrated gyroscope signal: 
  \begin{align}
    \mu(t) = 0, ~~
    \sigma^2(t) = \delta t \cdot t \cdot \sigma^2 \label{eq:gyro_error}
  \end{align}

\subsubsection{Accelerometer}
In contrast with gyroscopes, the signal needs to be integrated {\em twice} in order to arrive to the value of interest, position. This will give rise to more dramatic increases in position error with respect to time.  

\textbf{Static Bias} error $\epsilon_b$ will make the position error grow quadratically with time. 
    \begin{align}
      s(t) = \epsilon_b \cdot \frac{t^2}{2}
    \end{align}
    This bias can be estimated for a particular accelerometer experimentally. The classical method to do this is to first find the exact orientation of the accelerometer with respect to the gravity vector to subtract it and estimate the actual bias, this can be achieved by using calibration turntables. As an alternative, the device is calibrated using an ad-hoc method (see Sec. \ref{}). 

\textbf{Speed Random Walk}. As with the case of the gyro, the variance will increase with the square root of time. 
   
In order to evaluate the effect of the noise from the accelerometer on the estimation of position we can simply integrate {\em twice} the random variable with Gaussian distribution $\epsilon_g$. Nevertheless, the alternative expression used in \cite{thong_numerical_2004} and zero initial conditions are here assumed. Using the same notation as in Eq. \ref{eq:integ_noise}, we write position estimation as:
  \begin{align}
    s(T) = \int_0^T \left[ \int_0^t \epsilon_g(\tau)\, \mathrm{d}\tau \right] \mathrm{d}t 
  \end{align}
  Which can be integrated by parts to yield:
  \begin{align}
     s(T) &= T \int_0^T \epsilon_g(\tau)\, \mathrm{d}\tau - \int_0^T t~\epsilon_g(t) \, \mathrm{d}t \\
     &= \int_0^T(T-t)~\epsilon_g(t) \, \mathrm{d}t
  \end{align}
 By setting $T=\delta t(N-1)$ and $t = \delta t (n - 1)$, the term $(T - t)$ integrand becomes $(\delta t(N-1) - \delta t(n-1))$ and we can apply the simple rectangular rule to get
  \begin{align}
    s[n]^2 &= \delta t^4 \sum_{m=2}^N(N-m)\epsilon_g[m] \cdot \sum_{n=2}^N(N-n)\epsilon_g[n] 
  \end{align}
We exploit Gaussianity assumptions to retrieve an expected value and variance from the double integration of the random process:
  \begin{align}
    \mathrm{E}\left[ s[n] \right] &= \delta t^2\sum_{i=2}^n(n-i)\mathrm{~E}[N_i]=0 \\
    Var[s[n]] &= \delta t^4~\sigma_g^2\sum_{n=2}^N (N-n)^2 \\
    &=\frac{\delta t^4~\sigma_g^2}{6} (N-1)(N-2)(2N-3)
  \end{align}
 Replacing with the time of acquisition $N=T/\delta t + 1$ we get a function in the time domain:
  \begin{align}
    \sigma(t)^2 &= \frac{\delta t^4~\sigma_g^2}{6} (T/\delta t - 1)(2T/\delta t -1)T/\delta t
  \end{align}
  MEMS accelerometers have sampling frequencies is such that $T \ll \delta t$ Thus we arrive to the value of standard deviation with respect to time: 
  \begin{align}
    \sigma(T) \approx \sigma_g ~ T^{3/2} ~ \sqrt{\frac{\delta t}{3}} \label{eq:rms_accel}
  \end{align}
After $T$ seconds the signal of the accelerometer will introduce a random walk with zero mean and RMS value according to $T^{3/2}$.

\subsubsection{Strapdown Algorithm}
In order to estimate position, first the orientation must be known to project the acceleration input to a fixed inertial frame where gravity can be accounted for (Fig. \ref{fig:strapdown}).% Here the it is proven that when gyroscopic signal is solely used for orientation estimation, position will diverge. 
\begin{figure}[h]
  \centering
  \includegraphics[width=0.9\columnwidth]{figures/strapdown}
  \caption{Strapdown Inertial Navigation.}
  \label{fig:strapdown}
\end{figure}

Although careful calibration could remove accelerometer bias (the most obvious source of position drift), an error in orientation will generate a spurious compensated acceleration signal $\ddot{s}$ which when gravity is subtracted, will report an inexistent acceleration. When gyro alone is used for orientation estimation, small errors in orientation are  unavoidable because of white noise in the signal. 

The development by \cite{kuipers1999quaternions} is used where a rotational increment, $\mathbf{q}$, can be easily expressed as a quaternion multiplication
 \begin{align}
  \mathbf{q}_{n+1} &= \mathrm{exp}\left(0,~  -\frac{T}{2}\omega \right) \odot \mathbf{q}_n \label{eq:attUpdate} \\
  \mathrm{exp}(\mathbf{q}) &= \mathrm{exp}((0, q)) = \left( \cos \| q \|, ~\frac{q}{\|q \|}\sin \|q\| \right) \label{eq:cosFormula}
\end{align}
Consider a small tilt error $\delta \phi\mathbf{u}$ in the orientation estimation with $\delta \phi$ expressed in the body frame and where $\mathbf{n}$ is a vector close to the inertial vertical, where the direction of rotation $\mathbf{u}=[a~b~c]^t$ has $c \to 0$. 

For a static body, the error propagated because of a tilt error is given by
\begin{align}
  \epsilon_p = \mathbf{q}_{\delta\phi}\cdot \mathbf{g} \cdot \mathbf{q}_{\delta\phi}^{-1} - \mathbf{g} \label{eq:propError1}
\end{align}
Which, by using Taylor's first order linearization can be expressed as
\begin{align}
  \epsilon_p &= \left[ bg\delta\phi ~~ -ag\delta\phi ~~ -\left( a^2 + b^2 \right)(\delta\phi/2)^2\right] \\
  &=  \left[ bg\delta\phi ~~ -ag\delta\phi ~~ 0\right]
\end{align}
Since as the angle $\delta\phi$ is small, its square can be neglected. It can be shown that even the slightest error in tilt will yield a dramatic error in position: assume an error of $\delta \phi = 0.1^{\circ}$ around the axis $\mathbf{u}=[1~1~0]^t$. This introduces a non-existing acceleration of $ \epsilon_p = [\sqrt{2}g\delta\phi~~\sqrt{2}g\delta\phi~~0] = [0.02421~0.02421~0]~m/s^2$, which in turn gives rise to a position error of $ \mathbf{s} = \frac{1}{2}\cdot\epsilon_p\cdot t^2 = [1.21~1.21~0]m $ in only $10s$.
\section{RELATED WORK}
 \subsection{Visual-Inertial Fusion}
The complimentary nature of IMUs and cameras does bring an advantage to the pose estimation problem, but it also brings several difficulties of implementation. Most obviously, the need for a multi-rate scheme is called for, since the rate at which IMU measurements arrive can be up to an order of magnitude above the rate at which images become available by the sensor \cite{nutzi_fusion_2010}. Second, both sensors need to be calibrated in order for their measurements to be in agreement. Most of this calibration can be estimated at one single experiment for a given camera-IMU sensor pair. Lastly, even with the relative poses of the camera with respect to the IMU fully determined, for each experiment, the vision pose estimation will declare a different scale of the scene. In order to mix the poses of both sensors, an agreeing scale has to be found first.

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.9\columnwidth]{figures/loosely}
    \caption{Loosely Coupled Visual-Inertial fusion.}
    \label{fig:fusion}
\end{figure}

In the literature \cite{corke_introduction_2007}, two distinct classes of methods for estimating pose using the fusion of visual and inertial data are available: the \textit{loosely} and the \textit{tightly} coupled approaches. In a loosely coupled system, IMU and vision separately generate candidate poses of the rigid body, and the information is later shared at a high level only, either in cascading the estimations (pose of IMU is used as prior to the vision estimation, as in \cite{weiss_real-time_2012}) or by filtering both pose estimation's output. The tightly coupled system take into a single processing stage all available raw data for the multiple sensors and produce a single pose estimation, typically under a single state vector, as in \cite{domke_integration_2006}. For this work, the visual input is regarded as a black box to make the system more flexible. Because of this, a loosely coupled approach is proposed (Fig. \ref{fig:fusion}).

\subsection{Mobile Implementations}
Targeting specifically smartphones for pose estimation is not often discussed in the literature. Some exceptions exist, such as the pipeline from Klein and Murray \cite{klein2007parallel} which has been modified to accommodate the implementation in a smartphone, the iPhone 3 \cite{klein_parallel_2009}. The modifications done are only towards limiting the computational cost of the original pipeline and do not incorporate other senors available to the phone. 

The second attempt found in the literature, is from Porzi \textit{et. al.} \cite{porzi_visual-inertial_2012}. In this approach, again PTAM is implemented on a smartphone. They show improvements in accuracy for rapid rotations by leveraging the fast orientation estimation from the gyroscope and accelerometer with the accuracy of keyframe-based visual SLAM. Although this method exploits the inertial sensing capabilities of the phone, it only does so for attitude estimation and the scale is not recovered.

\section{METHODOLOGY}
From the strapdown algorithm analysis it is clear that classical visual-inertial fusion algorithms are not suitable for the high-noise conditions in current smartphones, so a loosely coupled robust scheme is followed to produce a more reliable and robust solution for such conditions.

Once initial estimation of biases as well as realtime considerations on PTAM are laid out, the steps taken for the integration of visual and inertial data are as follow:
\begin{enumerate}
  \item Initialize visual tracker pipeline.
  \item Use an Extended Kalaman Filter to integrate gyro, accelerometer and, when available, visual information to generate a stable orientation estimation updated at every new IMU data arrival.
  \item For every frame to frame motion segment, measure the non-gravitational acceleration projected in the inertial frame. 
  \item Robustly determine a scale from this corresponding visual and accelerometer-based displacements.
  \item Once the scale converges filter the pose estimated by the visual tracking pipeline. 
  \item Use inertial data to automatically acquire high-resolution images at suitable moments to be incorporated to the dense reconstruction solver. Whenever new frames are added, give audio feedback to the user.
  \item Display on-screen partial reconstruction and allow the user to save the results of the reconstruction. 
\end{enumerate}
This is described in detail in the following sections. The complete system is shown in Figure \ref{fig:final_system}.
\subsection{Optimizations}
Although the standard implementation of PTAM was used, several tuning to the parameters was performed to better accommodate the computational capabilities of the smartphone used. A key bottleneck in PTAM is the detection of salient point, done using the FAST \cite{rosten_machine_2006} detector. The detector is used in each pixel of every frame and in four different octaves, so the optimization of the FAST algorithm would be significant. This optimization was done using a Single Instruction Multiple Data (SIMD) engine available in the Galaxy S3, the NEON general-purpose SIMD engine.  

\subsection{Inertial Navigation Calibration}
Initial, rough estimation of biases in gyro and accelerometer are beneficial to the system to allow faster convergence to scale, which in turn leads to a faster filtering stage. In the case of the gyro, bias is experimentally obtained by acquiring several minutes of data and getting the mean of each axis. As for the accelerometer, the bias is expressed as a displacement in the observed dynamical range of each axis of the accelerometer. In unbiased conditions, the accelerometer would measure $\pm 1g$ on each axis when such axis is perpendicular to the vertical. Instead this range is corrupted by an additive bias which can be observed only when the full dynamical range is measured. By slowly rotating the phone while registering the values of the sensor, the extrema of the values for each axis are in fact the dynamical range. 

\subsection{Robust Metric Scale Estimation}
The operating conditions call for a robust system that:
\begin{itemize}
  \item Operates with motion \textbf{events}. Events are sections of displacement determined by {\em sufficiently exciting motion}. This automatically rejects small jerky hand motions. Each event has a data pair of IMU and vision displacements.
  \item Is capable of keep track of a large number of measured event pairs and incrementally reject spurious visual estimations. 
  \item Lastly, it should give a higher level of confidence based on the displacement in the inertial-frame vertical direction estimated by the IMU.
  \item Employs a decaying velocity model to mitigate small biases in the IMU signal.
\end{itemize}
Motion events are defined as short high-acceleration motion signals. They can easily be identified by segmenting the rotated, compensated, acceleration signal which is signaled by two opposite sign, similar magnitude peaks in a relatively short period of time. 

The decaying velocity model introduces an tunable parameter $\alpha$ with a value close to one to limit the contribution of an overestimated velocity to the position. Once the acceleration is small enough, it is assumed that velocity should approach zero (geometrically decay) since constant velocity is not possible for long distances in a hand-held scenarios.

Whenever a motion event is registered, the decaying velocity model is used to integrate the accelerometer segmented signal into a displacement. This displacement, $\hat{s}_i$, is then stored as the $k^{th}$ measurement pair along with the visual displacement counterpart in a {\em displacement pair queue}: $\{\hat{s}_i,\,\hat{s}_v\}^k$. The queue has a limited number of $N$ elements, in order to keep the outlier rejection process tractable. With each new entry to the queue $\mathcal{Q}$, a new candidate scale is computed from the newest pair alone. This candidate scale is checked against all the measurements in the queue. If the new scale has more inliers than the current best scale, a new scale is computed from the new inlier set $M$ that minimizes 
\newcommand{\argmin}{\operatornamewithlimits{argmin}}
\begin{align}
  \argmin_\lambda\sum_m^M(\hat{s}^m_i-\lambda\cdot\hat{s}^m_v)^2 \label{eq:lls_scale}
\end{align}
 If the new candidate scale does not produce more inliers than the current best estimate of $\lambda$, then the new measurement pair is kept in the queue and the old scale is checked against the candidate scale. If the new pair agrees with the old scale the inlier count is increased and the old scale adjusted using Eq. \ref{eq:lls_scale}. 
 \subsection{Visual-Inertial Fusion}
In order to account for bias drift, whenever there exist a reliable measurement from the visual pipeline that indicates that the motion of the body is close to zero, a Zero Velocity Update (ZUPT) takes place. The ZUPT will usually help to limit the uncertainty in as INS: measured values should reflect the zero velocity condition signaled by the ZUPT. 

Special consideration must be given to the timing of the ZUPT in order to validate the accuracy of the update. Since there is no guarantee that the time reported by the Android's API corresponds to the exact time of image acquisition nor is it consistent with the IMU timer, the delay between the visual information and the IMU information must be empirically determined. Also, for the actual filtering of the pose that will result as output when the scale converges, the relative timing of the IMU and vision poses is essential.

With each new vision position datum, the position given is first scaled to metric units and then the state of the integrator is updated using a Direct Filter, shown to exponentially converge given de-biased inertial values in \cite{baldwin_complementary_2007}:
\begin{align}
  \hat{v}^{k+1}_i &= R_B\vec{v}^{~k}_i+K_p R^t_B (\hat{s}^{~k}_i - \vec{s}^{~k}_i) \\
  \hat{s}^{k+1}_i &= \tau \hat{v}^{~k+1}_i + \hat{s}^{k}_i 
\end{align}
Where $K_p$ is the tunable parameter of the filter and it should lie somewhere between zero and Nyquist frequency of the sampling rate of the pose estimation \cite{baldwin_complementary_2007}.

\begin{figure}[h!]
  \centering
  \scalebox{.52}
  {
    \input{figures/dia/full_system_2}
  }
  \caption{Complete system workflow.}
  \label{fig:final_system}
\end{figure}


 \section{RESULTS} 
The evaluation of the  algorithm was performed by two methods:
\begin{enumerate}
  \item Using a limited set of features for visual tracking, compare the scaled and filtered pose against the external motion capture system (VICON).
  \item Measurement of a point cloud from a real-world object and comparison with its real measurements.
\end{enumerate}
\begin{figure}[h!]
  \centering
  \newlength\figurewidth 
  \newlength\figureheight 
  \setlength\figureheight{3cm} 
  \setlength\figurewidth{5cm}
  %\vspace{5mm}
  \input{figures/filter_effect.tex}
  \caption{Fusion effect on visual failure.}
  \label{fig:filter_effect}
\end{figure}
 Method 1 is intended to show how the system improves on the pose especially when the vision pipeline fails, whereas method 2 is intended to show how the estimated scale is accurate enough to be implemented reliably for end-user solutions and that the system is efficient enough to allow for a computationally-demanding process, such as dense reconstruction. Furthermore, method 2 also shows some exciting new capabilities \cite{Tankanen_3dlive_2013} that can be unlocked in new mobile technology. 
\subsection{Motion Capture} 
In this experiment the visual system is operating using a small number of keypoints in order to keep the computational time of the visual tracker more or less constant. This is done to isolate the performance of the system from the hang-ups that the visual system might introduce if the number of keypoints is too high.

\begin{figure}[h!]
   \centering
    \setlength\figureheight{5cm} 
    \setlength\figurewidth{5cm}
    %\vspace{5mm}
    \input{figures/traj_3D.tex}
  \caption{3D Trajectory.}
  \label{fig:traj_3d}
\end{figure}

The type of motion used for the scale estimation is, high-acceleration motion, preferably in the $z$-axis direction. The convergence of scale using this motion was achieved in less than 10 seconds. Since the rate of convergence was measured under this optimal operating conditions it represents a lower bound of the convergence time.

\subsection{Point Cloud}
The dense reconstruction pipeline used for these experiment was designed with the pose estimation method contributed herein as an underlying framework, however the pipeline is not part of this paper. A detailed description of the dense reconstruction pipeline is given in Tanskanen \textit{et. al} \cite{Tankanen_3dlive_2013}. In broad terms, the dense reconstruction is a stereo-based composed by a three step procedure: image mask estimation, depth map computation and depth map filtering. Finally the depth map is back-projected to 3D, colored with respect to the reference frame.

After the point cloud is obtained, a RANSAC based cylinder fitting procedure yields an optimal diameter for a circular cylinder. Afterwards the diameters are compared and thus the accuracy can be observed as in Figure \ref{fig:cylinders}.
\graphicspath{{figures/}}
\begin{figure}[h!]
  \centering
  \def\svgwidth{250pt}
  \input{figures/cylinders.pdf_tex}
  \caption{Point cloud comparison for scale accuracy.}
  \label{fig:cylinders}
\end{figure}
As it can be seen on Figure \ref{fig:cylinders}, the accuracy of the system is validated. The time to acquire the dense reconstructions of the textured cylinder would vary between 1 and 3 minutes of interactive usage. No postproccessing is performed. 

\section{CONCLUDING REMARKS}
This paper provides a novel framework for the integration of inertial and visual sensors that is applicable in low-power devices with low-grade inertial sensors; the case of current smartphones. The pipeline described does not use fiducial markers for the visual work, nor does it require any knowledge of the scene, and is capable of achieving realtime operation.  
\subsection{Conclusion}
The initial goals of the particular framework proposed were validated for pure pose estimation cases, as well as cases where the runtime is burdened with significant overhead from a dense reconstruction pipeline. To further assist the dense pipeline, a motion-event trigger was implemented. With the use of the motion-event trigger, the dense-frames can be automatically acquired, removing the need of the user to know exactly what frames will make the dense pipeline fail or succeed.  

The complete method was systematically tested to asses the quality of the pose filtering (Fig. \ref{fig:traj_3d}), the accuracy of the scale (Fig. \ref{fig:cylinders}) and the usability of the motion-event trigger for automatic dense-frame acquisition. The system was validated and proven to handle visual failures (Fig. \ref{fig:filter_effect}), achieve scales accurate to within a 8\% error and to enable the desired human-computer interaction for automatic dense reconstruction. 
\subsection{Future Work}
In order to lower energy consumption and allow better resolutions for the dense pipeline, a more principled approach is envisioned as future work. This approach would be targeted specifically for the mobile scenario from the start, and would fully take advantage of the sensor capabilities of the IMU in order to provide a more effective solution.  Finally, the information about the scene from the visual-inertial pipeline could be provided to the dense pipeline to build a more integrated and interdependent system. This, along with the above suggestion for future work, would defeat some of the modularity aspects of the current work, but would benefit from the increased performance and robustness. 

\bibliographystyle{plain}
\bibliography{refs}
\end{document}


