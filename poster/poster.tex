\documentclass[final]{beamer}
\usepackage{color}
\usepackage{etex}
\usepackage{pgfplots}
\mode<presentation>{\usetheme{Icy}}
\usepackage{times}
\usepackage{amsmath,amssymb}
\usepackage[english]{babel}
\usepackage[latin1]{inputenc}
\usepackage[default]{lato}
\usepackage[T1]{fontenc}
\usepackage[orientation=portrait,size=a0,scale=1.4]{beamerposter}
\usepackage{booktabs,array}
\usepackage{pstricks}
\usepackage{xspace}
\usepackage{listings}
\usepackage{fp}
\usepackage{ifthen}

\graphicspath{{figures/}}

\title{\Huge Realtime Visual-Inertial Pose \\[0.5ex]Estimation on a Mobile Phone}
\author{\hskip1em Federico Camposeco Paulsen}
\institute
{
  \hskip1em Le2i, University of Burgundy\\ \vskip0.5ex\hskip1em Institue for Visual Computing, ETH Zurich
}

\date[June. 20th, 2013]{June. 20th, 2013}

\begin{document}
\begin{frame}{} 
\vspace{-1cm}
\begin{columns}[t]
  \begin{column}{.45\linewidth}
    \begin{block}{Introduction}
      \begin{itemize}
        \item Recent faster IMU-equipped phones introduce exciting possibilities. 
        \item Monocular visual SLAM is only up-to-scale.
        \item Position estimation from IMU is unreliable at best.
        \item The method proposed:
          \begin{itemize}
            \item Robustly estimates the scale from visual SLAM
            \item Efficiently fuses inertial and visual data afterwards
            \item Provides an automatic frame acquisition for dense reconstruction.
            \item Fast enough to allow the dense reconstruction to happen on-board in realtime.
          \end{itemize}
      \end{itemize}
    \end{block}
    \begin{block}{Background: Visual SLAM}
      There are two types of visual SLAM
      \vskip1ex
      \begin{columns}[c]
        \begin{column}{0.5\linewidth}
          \raggedleft
        \includegraphics[width=0.6\linewidth]{monoslam_1}\\
        Filtering, like MonoSLAM
        \end{column}
        \hskip3em
        \begin{column}{0.5\linewidth}
          \raggedright
        \includegraphics[width=0.6\linewidth]{keyframe_1}\\
        Keyframe, like PTAM
        \end{column}
      \end{columns}
      \vskip1ex
      For speed-accuracy compromise, PTAM is chosen as reference implementation. 
    \end{block}
    \begin{block}{Background: Inertial Pose Estimation}
      \begin{itemize}
        \item Using IMU (gyro+accel) on board the Samsung GT-I9300. 
        \item IMU available is \alert{not} designed for position.
        \item Gyro used as orientation and accelerometer for position
      \end{itemize}
      \begin{centering}
        \includegraphics[width=0.7\linewidth]{strapdown}\\
      \end{centering}
    \end{block}
    \begin{block}{Background: Error Propagation}
      \begin{itemize}
        \item Biases and noise in accelerometer introduce position error.
        \item Same conditions in gyro produce orientation error.
        \item Biases can be calibrated and position error from accelerometer noise is small.
        \item However, even a 0.5$^{\circ}$ tilt error from orientation makes gravity compensation fail, and the error in position grows as much as 10m/s. 
      \end{itemize}
      \begin{centering}
        \includegraphics[width=0.4\linewidth]{errors}\\
      \end{centering}
    \end{block}
    \begin{block}{Background: Visual-Inertial Fusion}
      \begin{columns}[T]
        \begin{column}{0.5\linewidth}
          \begin{itemize}
            \item Two types of visual-inertial fusion coupling.
              \begin{itemize}
                \item Tightly: Treats camera as another sensor. Raw inputs enter a single filter.
                \item Loosely: Visual and inertial inputs generate a pose, poses are mixed.
              \end{itemize}
            \item Loosely coupled used here because of of flexibility.
          \end{itemize}
        \end{column}
        \begin{column}{0.5\linewidth}
        \begin{centering}
          \includegraphics[width=0.95\linewidth]{loosely}\\
        \end{centering}
        \end{column}
      \end{columns}
    \end{block}
  \end{column}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  \begin{column}{.45\linewidth}
    \begin{block}{Methodology: Optimizations}
       \begin{columns}[T]
        \begin{column}{0.5\linewidth}
          \begin{itemize}
            \item Main bottleneck is visual feature detection
            \item Utilize the vectorization SIMD available in ARM.
            \item Speedup of up to 500\%.
          \end{itemize}
        \end{column}
        \begin{column}{0.5\linewidth}
        \begin{centering}
          \def\svgwidth{550pt}
          \scriptsize
          \hskip-2em
          \input{figures/neon_fast.pdf_tex}\\
        \end{centering}
        \end{column}
      \end{columns}
    \end{block}
    \begin{block}{Methodology: Robust Metric Scale Estimation}
      \begin{columns}[t]
        \begin{column}{0.5\linewidth}
          \begin{itemize}
            \item Define IMU motion segments by signal segmentation.
            \item Enter IMU segments with their vision counterpart into a incremental random consesus scheme. 
            \item Extract metric scale while rejecting outliers.
          \end{itemize}
        \end{column}
        \begin{column}{0.5\linewidth}
          \centering
          \newlength\figureheight 
          \newlength\figurewidth 
          \setlength\figureheight{6cm} 
          \setlength\figurewidth{14cm}
          \tiny
          \vskip-2ex
          \input{figures/accel_events.tex}
          \setlength\figureheight{6cm} 
          \setlength\figurewidth{14cm}
          \tiny
          %\vskip5ex
          \input{figures/vision_outlier.tex}
        \end{column}
      \end{columns}
    \end{block}
    \begin{block}{Methodology: Proposed Workflow}
      \begin{columns}[]
        \begin{column}{0.35\linewidth}
          \begin{itemize}
            \item After scale converges, the poses can be fused.
            \item A direct filter is used because of efficiecy and ease of tunning.
            \item System gives metric scale, improved pose and motion events for external dense mapping. 
          \end{itemize}
        \end{column}
        \begin{column}{0.65\linewidth}
          \centering
          \scalebox{.85}{\tiny\input{figures/dia/full_system_2}}
        \end{column}
      \end{columns}
    \end{block}
    \begin{block}{Results}
      \begin{columns}[]
        \begin{column}{0.5\linewidth}
          \begin{itemize}
            \item Metric pose estimation verified against ground truth.
          \end{itemize}
          \raggedright
          \includegraphics[width=0.9\linewidth]{results3D}\\
        \end{column}
        \begin{column}{0.5\linewidth}
          \begin{itemize}
            \item Metric accuracy tested in a dense point cloud built in realtime.
          \end{itemize}
          \centering
          \def\svgwidth{550pt}
          \tiny
          \hskip-3em
          \input{figures/cylinders.pdf_tex}
        \end{column}
      \end{columns}
    \end{block}
    \begin{block}{Conclusions}
      \begin{columns}[]
        \begin{column}{0.5\linewidth}
          \begin{itemize}
            \item In spite of outliers, metric scale is recovered.
            \item VIsual failures are now handled.
            \item Efficiency is kept high to allow a costly 3D dense reconstruction
            \item Non-experts can acquire dense keyframes automatically.
          \end{itemize}
        \end{column}
        \begin{column}{0.5\linewidth}
          \centering
          \includegraphics[width=0.9\linewidth]{face}\\
        \end{column}
      \end{columns}
    \end{block}
  \end{column}
\end{columns}

\vfill
\end{frame}
\end{document}
