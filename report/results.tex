\chapter{Results} \label{chap:results}
The evaluation of the realtime algorithm presented in Section \ref{sec:proposed_method}, was performed by two methods:

\begin{enumerate}
  \item Using a limited set of features for visual tracking, compare the scaled and filtered pose against the external VICON motion capture system.
  \item Measurement of a point cloud from a real-world object and comparison with its real measurements.
\end{enumerate}

Although the success of both experiments rely on the convergence of scale, they show the applicability of the method for different purposes. Method 1 is intended to show how the system improves on the pose especially when the vision pipeline fails, whereas method 2 is intended to show how the estimated scale is accurate enough to be implemented reliably for end-user solutions and that the system is efficient enough to allow for a computationally-demanding process, such as dense reconstruction. Furthermore, method 2 also shows some exciting new capabilities \cite{Tankanen_3dlive_2013} that can be unlocked in new mobile technology. 


\section{Comparison Against VICON}
The motion of the smartphone was tracked externally using an infrared motion capture system, the VICON system \footnote{\url{http://www.vicon.com/products/vicontracker.html}}. Using markers attached to the phone, the pose of the device can be tracked in realtime (less than 2.5ms of latency) and with sub-millimeter accuracies. This makes the motion capture system perfect to generate a ground truth dataset to compare the estimated pose from the system. Although the $z$-axis direction of VICON is aligned with the inertial $z$-axis used, the $x$ and $y$ axes are not aligned. In order to compare the datasets, the yaw angle of the ground truth trajectory was modified to match the orientation of the visual-inertial frame of reference. 

In this experiment the visual system is operating using a small number of keypoints in order to keep the computational time of the visual tracker more or less constant. This is done to isolate the performance of the system from the hang-ups that the visual system might introduce if the number of keypoints is too high. This, in any way, is a limitation to the system, it was done solely for the purpose of this experiment. 


\begin{figure}[h!]
  \centering
  \begin{subfigure}[b]{1\textwidth}
    \centering
    \caption{Typical motion used for scale estimation.}
    \setlength\figureheight{4cm} 
    \setlength\figurewidth{10cm}
    \input{figures/scale_proc.tex}
    \label{fig:scale_proc}
  \vspace{6mm}
  \end{subfigure}
  \begin{subfigure}[b]{1\textwidth}
    \centering
    \caption{Scale evolution during scale estimation.}
    \setlength\figureheight{4cm} 
    \setlength\figurewidth{10cm}
    \input{figures/scale_evol.tex}
    \label{fig:scale_evol}
  \end{subfigure}
    \caption{Scale estimation procedure.}
\end{figure}

At the beginning, the output pose of the system is pure vision-based. This is, of course, unscaled and low frequency data, but as soon as the scale estimator converges, the system is able to robustly mix the output of IMU and vision pipelines. Therefore, the first step is to make the scale converge to a stable value. For this experiment, at the beginning the motion of the phone is guided to achieve convergence of the scale estimation (as in Figure \ref{fig:scale_proc}), but in real operation, the visual system can take over indefinitely until scale has converged. 

\begin{figure}[h!]
  \begin{subfigure}[h!]{0.33\textwidth}
    \centering
    \setlength\figureheight{3.3cm} 
    \setlength\figurewidth{3.3cm}
    \fbox{\input{figures/traj_3D_f.tex}}
    \caption{Front view.}
  \end{subfigure}~
  \begin{subfigure}[h!]{0.33\textwidth}
    \centering
    \setlength\figureheight{3.3cm} 
    \setlength\figurewidth{3.3cm}
    \fbox{\input{figures/traj_3D_s1.tex}}
    \caption{Side view.}
  \end{subfigure}~
  \begin{subfigure}[h!]{0.33\textwidth}
    \centering
    \setlength\figureheight{3.3cm} 
    \setlength\figurewidth{3.3cm}
    \fbox{\input{figures/traj_3D_t.tex}}
    \caption{Top view.}
  \end{subfigure}
  \begin{subfigure}[h!]{1\textwidth}
    \centering
    \setlength\figureheight{10cm} 
    \setlength\figurewidth{10cm}
    \vspace{5mm}
    \input{figures/traj_3D.tex}
  \end{subfigure}
  \caption{3D Trajectory.}
  \label{fig:traj_3d}
\end{figure}

The type of motion used for the scale estimation is, as stated before, (relatively) high-acceleration motion, preferably in the $z$-axis direction. Thus, the rate of convergence was measured under this operating conditions and, as such, it represents a lower bound of the convergence time. Figure \ref{fig:traj_3d} shows the result of the visual-inertial fusion compared with the VICON ground truth. The motion of the phone was achieved free-hand while tacking a table-top scenario. 

At each new vision event (see Sec \ref{sec:proposed_method}) the latest scale hypothesis is recorded. The evolution of such scale, as well as the time of convergence can be observed on Figure \ref{fig:scale_evol}. Although there is no easy way to compare the time of convergence, it may suffice to mention that the time of convergence reported by Weiss and Siegwart \cite{weiss_real-time_2011} about 80 seconds of time of convergence when sufficiently high motion is present. The system proposed here, although operated by an expert, converges in much less time and without any constrain on the initial guess of scale, as opposed to \cite{weiss_real-time_2011}.

\begin{figure}[h!]
    \centering
    \setlength\figureheight{6cm} 
    \setlength\figurewidth{10cm}
    \vspace{5mm}
    \input{figures/filter_effect.tex}
    \caption{3D Trajectory.}
    \label{fig:filter_effect}
\end{figure}

One of the most important features of the contributed method, is its ability to overcome errors in the visual pipeline pose estimation. This is accomplished by the detecting errors using two criterion. First the system will use the tracking quality reported by the visual pipeline, however, the quality reported will be delayed by some undetermined time. The second criteria is to check the orientation innovation term, since the orientation from the IMU is in fact performed in a more robust filter, a high innovation would suggest a drift in the update. Using this, a failure from the visual pipeline can be detected and the Direct Filter can act accordingly by modifying the term $K_p$. Visual failure was enforced by simply blocking the camera's view by some seconds and comparing the pure visual pose estimation with the fusion estimation. Results of this are shown in Figure \ref{fig:filter_effect}. It can be seen that the, although the fused pose indeed suffers from visual failure (around second 89), it does not dramatically do so as with the pure visual pose estimation.
  
\section{Accuracy Evaluation Using Dense Point Clouds}
In order to verify that in the presence of more visual features the system holds, as well as to show the capabilities of the system in more user-oriented operations, the accuracy of the system is tested by comparing measurements in the scaled point cloud against real-world measurements. 

The dense reconstruction pipeline used for these experiments was designed with the pose estimation method contributed herein as an underlying framework, however the pipeline is not part of this dissertation. A detailed description of the dense reconstruction pipeline is given in Tanskanen \textit{et. al} \cite{Tankanen_3dlive_2013}. In broad terms, the dense reconstruction is a stereo-based composed by a three step procedure: image mask estimation, depth map computation and depth map filtering. Finally the depth map is back-projected to 3D, colored with respect to the reference frame.

In general the dense pipeline will give some a noisy point cloud, given that no postprocessing is performed to the 3D data. Because of this, the accuracy evaluation was performed using a perfect cylinder. A cylinder is chosen because of the ease of parameter estimation and of real-world measurements. After the point cloud is obtained, a RANSAC based cylinder fitting procedure yields an optimal diameter for a circular cylinder. Afterwards the diameters are compared and thus the accuracy can be observed as in Figure \ref{fig:cylinders}.

\begin{figure}[h!]
  \centering
  \def\svgwidth{270pt}
  \input{figures/cylinders.pdf_tex}
  \caption{Point cloud comparison for scale accuracy.}
  \label{fig:cylinders}
\end{figure}

As it can be seen on Figure \ref{fig:cylinders}, the accuracy of the system is indeed validated. The time to acquire the dense reconstructions of the textured cylinder would vary between 1 and 3 minutes of interactive usage. As mentioned, no postproccessing is performed. 

The acquisition of the frames necessary for the dense reconstruction pipeline were selected automatically, as depicted in Figure \ref{fig:final_system}. This is the last feature tested in the system, the detection of motion events by the Robust Scale Estimator. At each of those events a frame for the dense pipeline will be acquired.  Not only does the automatic acquisition assist the user in taking the necessary frames for the dense reconstruction, but the same motions needed for the automatic acquisition are beneficial to the scale estimation; they represent sufficiently exciting motion. For Figure \ref{fig:face} the scale and reconstruction was completed in realtime using less than a minute for the experiment. A video is also supplied in order to properly show the effect of this Human Computer Interaction innovation.

\begin{figure}[h!]
  \centering
  \includegraphics[width=0.8\linewidth]{figures/face.png}
  \caption{3D dense reconstruction built on top of the proposed system.}
  \label{fig:face}
\end{figure}



