\chapter{State of the Art} \label{chap:stateOfArt}
  \section{Realtime Structure from Motion}\label{sec:realtime_sfm}
  Current state-of-the-art SfM algorithms have largely solved the problem at hand. Some focus in the integration of heterogeneous and unordered set of images processed in batch (see \cite{frahm2010building}), while others focus on sequential or video feed processed online, or recursively. The later are much more relevant to this thesis since the platform to be addressed here will provide a {\em video feed} to the pipeline and a realtime pose and map estimation is to be obtained. Although several approaches to online SfM exist (as discussed in Sec. \ref{sect:intro:visTrack}), here we make emphasis on PTAM by Klein and Murray \cite{klein2007parallel} for the case of keyframe methods and MonoSLAM by Davison \textit{et. al} \cite{davison_monoslam:_2007}. 

After the methods are briefly presented, Section \ref{sec:mobile_sfm} introduces the advances of SfM for mobile devices. 

  \subsection{Parallel Tracking and Mapping}
 In this system proposed by Klein and Murray the task of performing realtime SLAM/SfM is divided into two separate and parallel processes, or in computer terms, threads. As presented in their original paper, the method can be summarized into the following:
 \begin{itemize}
   \item Tracking and mapping processes are separated and run in parallel.
   \item The mapping process will rely on keyframe selection to perform bundle adjustment on them. 
   \item Tracking process will rely on the actual map to localize the camera at each time step. Ergo, the map must be first initialized somehow in order for the tracking to begin.
   \item Map get initialized by a stereo pair (usual epipolar pipeline see \cite{Hartley2000}).
   \item Map can grow efficiently by epipolar search. 
 \end{itemize}
 Such scheme allows for the mapping side to not be disturbed or modified at all whenever a new frame arrives, and only process new data in the form of {\em keyframes}, as opposed to the filtering methods that modify all the landmarks at each frame (at rates of at least 20ms between frames). The efficiency and realtime capabilities of the system come from not only reducing the computational complexity of the SLAM/SfM problem, but relegating the remaining costly functions to a thread separate from the tracking thread. 

 The increased robustness of the system is justified in the context of monocular augmented reality applications. The hand-held scenario is far more challenging than a ground mobile robot and requires a new method to deal with the data-association issues that would arise from the complex and noisy motion that is dealt with in a hand-held scenario. In order to give priority to realtime local pose estimation, a separation between the mapping and the tracking is a valid solution. Further, the background process of mapping can be done more carefully in a batch process (using bundle adjustment) instead of in an open-loop incremental fashion.  
  
 \subsubsection{Tracking}
Once the map has been initialized, the tracking thread can then use the initial map points to localize the camera. This localization is done at each new frame in realtime and the pose obtained is proposed to be used for AR applications. For each new frame acquired by the camera, a prior pose estimate is generated using a decaying velocity motion model. Using this prior, the map points are projected to the putative location of the camera. Using a coarse-to-fine approach, only 50 coarse points are searched for in order to get a preliminary pose update by minimizing the reprojection error introduced by the prior pose. After this second preliminary pose is obtained, a larger number of points (1000) is reprojected and a new pose update is performed. The location of salient image-points at each step is determined used the FAST algorithm (Features from Accelerated Segment Test). This detection technique will be later re-implemented to speed-up the execution of PTAM, and so, it is explained in detail in Section \ref{sec:FAST}. The pipeline described above can be seen in Figure \ref{fig:track_thread}.

\begin{figure}[h!]
  \centering
  \scalebox{.93}
  {
    \scriptsize
    \input{figures/dia/ptam_track}
  }
  \caption{PTAM tracking thread.}
  \label{fig:track_thread}
\end{figure}

 \subsubsection{Mapping}
The mapping thread is in charge of taking the new keyframes and adding their observed new map points into the map, as well as refining the current map via bundle adjustment. As opposed to the tracking thread, the mapping needs to first be initialized in some form. 

The original PTAM algorithm proposes a stereo initialization using the five-point algorithm \cite{stewenius2006recent}. This is done by having the user initialize the program by notifying where a first keyframe is to be considered. The user then {\em smoothly} translates the camera and when enough baseline is present, notifies the program. The mapper then uses the first and last images of the tracking as keyframes and since the motion was smooth, the tracks for several keypoints are used as correspondences for the five-point algorithm. A plane is then fitted to the initial map (we assume a dominant plane) using RANSAC \cite{fischler1981random}. 

After the initialization, the tracking part can now begin and new keyframes (so far there are only two) can be added. The current frame used for tracking is used as a keyframe if a heuristic set of conditions is met:
\begin{itemize}
  \item Tracking quality must be good.
  \item There must be at least 20 frames of separation between the current frame and the last keyframe.
  \item The camera must be a minimum distance from the nearest keypoint in the map
  \end{itemize}
The distance criteria is in fact determined by a parameter in (putative) metric since it is assumed that the distance between the initialization keyframes is 10cm. This heuristic conditions not only manage to keep the computational cost of the non-linear optimization in check, but it prevents degenerate cases such as stationary cameras corrupting the map.

After the keyframe is selected, the FAST keypoint measurements against map points and the estimated pose from the tracker is used as input to the map growing process. In the tracking thread, not every map point was reprojected into the frame because of time constrains. Since this thread is more asynchronous, all map points are reprojected and matched with all the FAST keypoints from the tracking thread, and new measurements are stored. In order to actually use the FAST keypoints from the tracker as map points, many keypoints must be rejected. In short, non-maximal, duplicate or outlier keypoints are not triangulated. The triangulation takes the closest keyframe in the map (since it knows the extrinsics of both images) and reject those points not having a match found using the epipolar constraint: the outliers. Lastly, the matching points are triangulated to the map. 

\begin{figure}[h!]
  \centering
  \scalebox{.97}
  {
    \scriptsize
    \input{figures/dia/ptam_map}
  }
  \caption{PTAM mapping thread.}
  \label{fig:map_thread}
\end{figure}


Of course, bundle adjustment is performed whenever the mapping thread is not busy adding more keyframes to the map. Bundle adjustment tries to adjust the poses  of the keyframes and the map point locations using the measurements of the map points of each keyframe in a non-linear optimization scheme based on the Levenberg-Marquardt method. This is a very costly process and it limits the amount of keyframes that the mapping thread can handle in practice, \textit{e.g} tens of seconds are required for 150 keyframes. To mitigate this, a local bundle adjustment is ran with higher priority than the global bundle adjustment. This local optimization only takes the current and four nearest keyframes into account. This method effectively limits the time of bundle adjustment and thus ensures that each new keyframe is optimized at least once against it neighboring keyframes.

Whenever no new keyframes are added and the bundle adjustments have converged, the mapping thread does not become idle. Instead, the map is improved by by making new measurements in old keyframes. This is possible since the map is assumed to have grown since the old keyframe was added, and this new map points are reprojected and matches are searched for. More data association refinements are performed using the novel map points to ensure that the tracking and mapping will improve over time. A diagram detailing the whole mapping process is shown in Fig. \ref{fig:map_thread}.

\subsection{Features from Accelerated Segment Test (FAST)} \label{sec:FAST}
FAST is a corner detection method designed for computational efficiency for realtime applications, specifically, vision-based augmented reality. The method of detection is quite simple, however, to achieve the performances reported in the original publication from Rosten and Drummond \cite{rosten_machine_2006}, Machine Learning techniques are used for a given dataset to train the order in which the tests are applied \cite{rosten_faster_2010}, this is clarified in the following.  
\begin{figure}[h!]
  \centering
  \includegraphics[width=0.7\textwidth]{figures/fast_detector.png}
  \caption{FAST detector support ring of 16 around test pixel $p$. \protect\footnotemark}
  \label{fig:fast_detector}
\end{figure}
\footnotetext{Taken from \cite{rosten_machine_2006}}
The algorithm will do a saliency test for each of the pixel in the image. In it standard implementation, the detector will use a one-pixel ring of 16 pixels around the test location, as shown in Figure \ref{fig:fast_detector}. This set of pixels $S_{16}$ is used to classify each test pixel $p$ as a salient point, or corner. Given the intensity $I_p$ of pixel $p$ and a given threshold $t$, the tests for saliency defined by FAST can be written as:

\begin{description}
  \item[Test 1] The set $A \subseteq S_{16}$ has $n$ contiguous (8-connected) pixels such that $\forall q \in A \,:\, I_q - t > I_p$
  \item[Test 2] The set $B \subseteq S_{16}$ has $n$ contiguous (8-connected) pixels such that $\forall q \in B \,:\, I_q + t < I_p$
\end{description}
If either Test 1 or 2 are passed, the test pixel $p$ is classified as a feature. This kind of detection is known as a segment test, and similar approaches have been used an known prior to FAST. The real contribution of this method is the development of an efficient way of rapidly discarding non-corners by the use of machine learning techniques.  

Intuitively, the higher $n$ is set, the easier it will be to check weather $p$ is a corner or not. For the version without machine learning enhancements, the usual number of contiguous pixels satisfying the condition is set to 12, since with 12 connected components it is trivial to effectively speedup the test. In such scenario, it is sufficient to check the minimal set $M$ of pixels in locations 1,4,9 and 13 (see Figure \ref{fig:fast_detector}) to initially reject a candidate. 

In order for either Test 1 or 2 to be possible, {\em at least} 3 pixels of $M$ must be outside the threshold region (either darker by at least $t$ or brighter by at least $t$), otherwise no 12-pixel chain of the same type can be contiguous. Once this minimal set is tested as true, the rest of the pixels are checked. This speeds up the detection since most of the pixels that are not features will be discarded in the first step. There are two main disadvantages in using this approach: (1) since this trivial speed-up is not generalized for $n<12$, and (2) the selection of pixels 1,4,9 and 13 is arbitrary and the best choice would depend on the average appearance of features in a dataset. 

Addressing these two weaknesses, the authors of FAST developed a machine learning algorithm in which, for a given set of training images the optimal locations to check first were opbtained using the Maximum Information Gain criterion in an ID3 Classification Algorithm. This way, an optimal (dataset specific) ordering of the candidate rejection tests was calculated for $n={9,\dots,12}$. The corners detected using the FAST algorithm would slightly differ from the brute-force segment test, as the FAST algorithm is dependent on the dataset it was trained with. 

Using FAST with $n=9$ the authors report execution times 23 times faster than the common Harris detector and slightly better repeatability \cite{rosten_machine_2006}.

  \subsection{Mobile Implementations}\label{sec:mobile_sfm}
  Targeting specifically mobile platforms with a resource limitation similar to a smartphone has not been so widely discussed in the literature. In fact, the literature regarding the specific area of smartphones is largely inexistent. That is not to say that the literature is completely devoid of such attempts. In particular, the pipeline from Klein and Murray \cite{klein2007parallel} has been in fact modified to accommodate the implementation in a smartphone, the iPhone 3 \cite{klein_parallel_2009} (see Fig. \ref{fig:ptam_phone}). The modifications done are only towards limiting the computational cost of the original pipeline and do not incorporate other senors available to the phone. Furthermore, the results of the paper cannot be easily verified, as the code for this version of PTAM, MiniPTAM, has not been released to the public. 

The second attempt found in the literature that concerns the problem stated in this dissertation, is from Porzi \textit{et. al.} \cite{porzi_visual-inertial_2012}. In this approach, again PTAM is implemented on a mobile platform, this time an Android device.They show improvements in accuracy for rapid rotations by leveraging the fast orientation estimation from the gyroscope and accelerometer, with the accuracy of keyframe-based visual SLAM. Although this method exploits the inertial sensing capabilities of the phone, it only does so for attitude estimation. 

This two attempts are aimed at the implementation of algorithms that will enable the smartphone to act as a Virtual or Augmented Reality (AR) platform. The final goal of AR is to render and display graphics on the user display as though the rendered objects were in fact there. This requires accuracy and realtime execution in the tracked camera pose, while the accuracy, metric scale and completeness of the map are not considered. Although accurate pose tracking is of paramount importance to AR applications, in this thesis additional objectives are laid out: metric scale estimation, full 6DoF pose filtering using the IMU and automatic keyframe acquisition for an additional dense reconstruction pipeline. 

Some attempts that focus on the 3D realtime reconstruction on mobile phones are also scarcely found in the literature. One of the few examples is in Kolbe \textit{et al.} \cite{kolbe_3d_2011}, which requires extensive user interaction, to the point of become more of an interactive modelling software than vision-based reconstruction. Another example is the marker-based reconstruction on for mobile phones from Hartl \textit{et al.} \cite{hartl_rapid_2011} and from Pan \textit{et al.} \cite{pan_rapid_2011}. This algorithm is based on shape-from-silhouette using voxel carving. Although the algorithm performs the reconstruction and provides realtime feedback of the reconstruction to the user, the method is limited to small objects and constrained to be positioned over a AR marker (see Fig. \ref{fig:voxel_phone}). 

\begin{figure}[h!]
  \begin{subfigure}[b]{0.5\textwidth}
    \centering
    \includegraphics[width=0.4\textwidth]{figures/iphone_ptam.png}
    \caption{MiniPTAM running on an iPhone3G. \protect\footnotemark}
    \label{fig:ptam_phone}
  \end{subfigure}~
  \centering
  \begin{subfigure}[b]{0.5\textwidth}
    \centering
    \includegraphics[width=0.8\textwidth]{figures/voxel_carve_phone.png}
    \caption{Voxel-carving on a smartphone. \protect\footnotemark}
    \label{fig:voxel_phone}
  \end{subfigure}
\end{figure}
\footnotetext{Image taken from \cite{klein_parallel_2009}}
\footnotetext{Image taken from \cite{hartl_rapid_2011}}

Several other methods have centered around the exploitation of rolling shutters for the solution of SfM, \textit{e.g.} Hedborg \textit{et. al.}. In this work, the data acquired for testing of their ``Rolling Shutter Bundle Adjustment'' is done using an iPhone4, but the processing was done offline and no other sensors are exploited. This method exploits the fact that a rolling shutter will capture the scene at different points in time on different sides of the sensor. The method is intended for fast high-res cameras, small accelerations and known models. Approaches such as MiniPTAM---far from exploiting this fact---try to overcome the inaccuracies from the invalid global shutter assumption, while maintaining a more general approach compared to Hedborg's. 

 \section{Visual-Inertial Fusion}
As aforementioned, having weight and processing power constrains, the field of mobile robotics has had several important advances in efficient approaches to the SfM/SLAM task. In particular, autonomous Micro Air Vehicles (MAVs) have even more restrictive payload and power consumption limitations, and as such, most advances in the robotics community towards monocular realtime SLAM improvements come from trying to incorporate cameras as a central sensing device. However, this advances are done in the context of a mobile robotics sensor platform, where the existence of IMUs is ubiquitous, and thus, their advances are related to the {\em integration} of visual and inertial data, as discussed here. 

Visual and inertial data are highly complimentary modes of egomotion tracking, but by themselves they present several difficulties and/or limitations. Vision alone has slow update rates and pose estimation is not smooth. The computaional cost is high and the estimations are only up to a scale factor. Inertial data alone suffers from very important drift because of noise in the gyroscopes and in the accelerometers, as seen on Section \ref{sec:errorsBIMU}. Image measurements, however, would prevent such drift from occurring, while inertial pose measurements can introduce further robustness in the ``blind'' inter-frame periods of time, while providing a notion of global scale and, as a result, reduce the computational cost.

The complimentary nature of IMUs and cameras does bring an advantage to the pose estimation problem, but it also brings several difficulties of implementation. Most obviously, the need for a multi-rate scheme is called for, since the rate at which IMU measurements arrive can be up to an order of magnitude above the rate at which images become available by the sensor. Second, both sensors need to be calibrated in order for their measurements to be in agreement. Most of this calibration can be estimated at one single experiment for a given camera-IMU sensor pair. Lastly, even with the relative poses of the camera with respect to the IMU fully determined, for each experiment, the vision pose estimation will declare a different scale of the scene. In order to mix the poses of both sensors, an agreeing scale has to be found first. 
\setcounter{subfigure}{0}
\begin{figure}[h!]
  \begin{subfigure}[b]{0.5\textwidth}
    \centering
    \raisebox{1cm}{\includegraphics[width=1\textwidth]{figures/tightly_coupled.png}}
    \caption{}
  \end{subfigure}~
  \centering
  \begin{subfigure}[b]{0.5\textwidth}
    \centering
    \includegraphics[width=1\textwidth]{figures/loosely_coupled.png}
    \caption{}
  \end{subfigure}
  \caption{Recursive approaches to visual-inertial fusion: (a) is a tightly coupled approach and (b) a loosely coupled approach \protect\footnotemark}
  \label{fig:vis-imu-approaches}
\end{figure}
\footnotetext{Image taken from \cite{corke_introduction_2007}}


In the literature, two distinct classes of methods for estimating pose using the fusion of visual and inertial data are available. The first class is the \textbf{batch} method, which has not received much attention from the robotics community recently, nevertheless the original method described in this thesis draws some concepts from this legacy method. The method focuses on relating the inertial data between a collection of frames or keyframes and minimizing a joint error function that penalizes trajectories between keyframes that do not agree with inertial data, such as the method of Jung and Taylor \cite{jung2001camera}. Although, this batch process is more aimed at an offline process, it can be used in realtime over a window of operation for the last few keyframes. 

The second class of approaches are the more intuitive and ubiquitous \textbf{recursive} algorithms. Inside this class of online approaches, there exist several degrees of integration of the Visual-Inertial framework (see \cite{corke_introduction_2007}), namely, the \textit{loosely} and the \textit{tightly} coupled approaches (Fig. \ref{fig:vis-imu-approaches}). In a loosely coupled system, IMU and vision separately generate candidate poses of the rigid body, and the information is later shared at a high level only, either in cascading the estimations (pose of IMU is used as prior to the vision estimation) or by filtering both pose estimation's output. The tightly coupled system take into a single processing stage all available raw data for the multiple sensors and produce a single pose estimation, typically under a single state vector. 

\subsection{Tightly Coupled Fusion}

In general, tightly coupled systems will integrate all raw sensor data into a single estimation filter. This results into a SLAM-style estimation problem, where the usual pose vector $\mathrm{x}^c$ is augmented by the state of the world, \textit{i.e.} all map points explored: $\mathrm{x}^w=[P_1~P_2~\cdots P_n]$. The update of the state is through the observation of the world features on the image plane as well as the inertial state of the camera (acceleration and angular rate). Furthermore, the state can easily be augmented for the inclusion of other calibration parameters and even the metric scale factor between the camera and the inertial frame of reference. A critical difficulty in the implementation of these type of monolithic solutions is the milti-rate nature of the problem, where IMU measurements arrive at dramatically different speeds. Regardless of this, some implementations of this approach exist, such as Strelow's \cite{strelow_motion_2004} or the one from Nutzi and Weiss \cite{nutzi_fusion_2010}, that propagates the state whenever a new measurement (IMU or image) is available, and has different update modes depending on the source of the update:
\begin{align}
  \mathrm{x}_{t+1}&=f(\mathrm{x}_t)~+\mathcal{N}(0,\mathcal{Q})  \\
  \mathrm{y}_{t}^i&=h^i(\mathrm{x}_t)+\mathcal{N}(0,\mathcal{R}^i)\label{eq:multi_rate_i}\\
  \mathrm{y}_{t}^v&=h^v(\mathrm{x}_t)+\mathcal{N}(0,\mathcal{R}^v)\label{eq:multi_rate_v}
\end{align}

Where $\mathrm{x} = [\,\mathrm{x}^c~|~\mathrm{x}^w]$. This is validated by assuming no certainty of the unobserved quantities at time $t$; the variance of the unavailable measurement at one given update is considered as infinite, and thus, the update of the filter only affects the corresponding elements in the state vector. Other---more principled---approaches implementing high-order holding for one measurement at the presence of the other for a given update, or the weighting of the updates in Eqs. \ref{eq:multi_rate_i} and \ref{eq:multi_rate_v} as a function of their update frequencies, are suggested by more classical multi-rate filtering literature. A reference solution to the tightly-coupled visual-inertial fusion was done by Jones and Soatto \cite{jones_visual-inertial_2011}, a notable result reported for their contribution compared to vision-only is shown in Figure \ref{fig:vis-imu-approaches}. 

  \begin{figure}[h!]
    \centering
    \includegraphics[width=0.8\textwidth]{figures/jones_soatto.png}
    \caption{Comparison of visual-inertial (right) navigation against (manually scaled) visual-only. \protect\footnotemark}
    \label{fig:vis-imu-approaches}
  \end{figure}
  \footnotetext{Image taken from: \cite{tsotsos_visual-inertial_2012}}

  Another notable and recent example of a tightly coupled method is the one described by Bleser and Stricker in \cite{bleser_using_2008} where an MPF (Marginalized Particle Filter) is used to fuse inertial and visual measurements. Even more recently, this MPF approach was later coupled with a secondary vision estimation using Visual Odometry \cite{bleser_using_2010}. 

  \subsection{Loosely Coupled Fusion}
Under this scheme, the SfM and the INS algorithms run independently for the most part. The interaction between both processes may occur in different ways, but they all share the fact that signals from either visual or inertial sensors undergo an independent pre-processing stage. The output of an INS system might be used as prior knowledge of the location of features or map points in the image, and the output of the visual pose estimator might be used to estimate IMU biases as in Corke \cite{corke2004inertial}. 

In the usual SfM/SLAM problem, feature comparisons are usually searched for using assumptions of small image-plane motions of such features from frame to frame. As is the case with PTAM, it assumes a decaying velocity model for the reprojection of map points into a putative image-plane location. When an IMU is available, frame to frame motion can be more accurately determined using the fast-rate information from the accelerometers, and the feature correspondences can be more efficiently searched.

One step further in the use of the IMU in the estimation process is the one proposed by Domke and Aloimonos \cite{domke_integration_2006}, which uses a stochastic approach to feature matching (see Fig. \ref{fig:stochastic_matching}).

Another way can be to abstract the visual sensor further, is to consider the calculated pose from the visual pipeline as an update to a Kalman-type filter, whose input $\mathbf{u}(t)$ is given by raw or preprocessed IMU data, as in Weiss and Siegwart \cite{weiss_real-time_2011}. With the increase in computational capabilities and refinement of realtime monocular SLAM algorithms (such as in Sec. \ref{sec:realtime_sfm}), this type of approaches have gained a lot of attention. 
\begin{figure}[h!]
  \centering
  \includegraphics[width=0.6\textwidth]{figures/stochastic_matching.png}
  \caption{Stochastic feature matches supported by an IMU.}
  \label{fig:stochastic_matching}
\end{figure}

In the work of Weiss and Siegwart \cite{weiss_real-time_2011}, the state of the EKF that mixes visual pose estimation---which is a modified version of PTAM---and inertial measurements, include the calibration between camera and IMU, the internal calibration of the IMU sensors and the metric scale of the visual reconstruction. The advantage of this approach is that the complexity of the overall system is independent of the visual pose estimator, which is abstracted into a black-box and can be changed with any other SfM algorithm. Although the reported results of the algorithm are very promising, they rely on the initial parameters to be estimated be close enough to the real values. Furthermore, the testbed used for the experiments is an MAV, whose vertical does not experience such diverse orientations and unconstrained motions as a handheld application. In this case, the input to the EKF system would not provide valid information for the overall system and the scale could not be recovered without a good initial estimate.

In theory, tightly coupled approaches present a more reliable solution compared to loosely coupled approaches, as the full state of the SLAM includes cross-couplings between all the observed features and the pose of the camera. However, this implementations pose very restrictive parameters on the hardware they can run on. Because of this, the loosely coupled approach is adopted for the purpose of implementing it as a practical solution on a smartphone. 
