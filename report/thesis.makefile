ALL_FIGURE_NAMES=$(shell cat thesis.figlist)
ALL_FIGURES=$(ALL_FIGURE_NAMES:%=%.pdf)

allimages: $(ALL_FIGURES)
	@echo All images exist now. Use make -B to re-generate them.

FORCEREMAKE:

include $(ALL_FIGURE_NAMES:%=%.dep)

%.dep:
	touch $@ # will be filled later.

ext-tikz/thesis-figure0.pdf: 
	lualatex -shell-escape -halt-on-error -interaction=batchmode -jobname "ext-tikz/thesis-figure0" "\def\tikzexternalrealjob{thesis}\input{thesis}"

ext-tikz/thesis-figure1.pdf: 
	lualatex -shell-escape -halt-on-error -interaction=batchmode -jobname "ext-tikz/thesis-figure1" "\def\tikzexternalrealjob{thesis}\input{thesis}"

ext-tikz/thesis-figure2.pdf: 
	lualatex -shell-escape -halt-on-error -interaction=batchmode -jobname "ext-tikz/thesis-figure2" "\def\tikzexternalrealjob{thesis}\input{thesis}"

ext-tikz/thesis-figure3.pdf: 
	lualatex -shell-escape -halt-on-error -interaction=batchmode -jobname "ext-tikz/thesis-figure3" "\def\tikzexternalrealjob{thesis}\input{thesis}"

ext-tikz/thesis-figure4.pdf: 
	lualatex -shell-escape -halt-on-error -interaction=batchmode -jobname "ext-tikz/thesis-figure4" "\def\tikzexternalrealjob{thesis}\input{thesis}"

ext-tikz/thesis-figure5.pdf: 
	lualatex -shell-escape -halt-on-error -interaction=batchmode -jobname "ext-tikz/thesis-figure5" "\def\tikzexternalrealjob{thesis}\input{thesis}"

ext-tikz/thesis-figure6.pdf: 
	lualatex -shell-escape -halt-on-error -interaction=batchmode -jobname "ext-tikz/thesis-figure6" "\def\tikzexternalrealjob{thesis}\input{thesis}"

ext-tikz/thesis-figure7.pdf: 
	lualatex -shell-escape -halt-on-error -interaction=batchmode -jobname "ext-tikz/thesis-figure7" "\def\tikzexternalrealjob{thesis}\input{thesis}"

ext-tikz/thesis-figure8.pdf: 
	lualatex -shell-escape -halt-on-error -interaction=batchmode -jobname "ext-tikz/thesis-figure8" "\def\tikzexternalrealjob{thesis}\input{thesis}"

ext-tikz/thesis-figure9.pdf: 
	lualatex -shell-escape -halt-on-error -interaction=batchmode -jobname "ext-tikz/thesis-figure9" "\def\tikzexternalrealjob{thesis}\input{thesis}"

ext-tikz/thesis-figure10.pdf: 
	lualatex -shell-escape -halt-on-error -interaction=batchmode -jobname "ext-tikz/thesis-figure10" "\def\tikzexternalrealjob{thesis}\input{thesis}"

ext-tikz/thesis-figure11.pdf: 
	lualatex -shell-escape -halt-on-error -interaction=batchmode -jobname "ext-tikz/thesis-figure11" "\def\tikzexternalrealjob{thesis}\input{thesis}"

ext-tikz/thesis-figure12.pdf: 
	lualatex -shell-escape -halt-on-error -interaction=batchmode -jobname "ext-tikz/thesis-figure12" "\def\tikzexternalrealjob{thesis}\input{thesis}"

ext-tikz/thesis-figure13.pdf: 
	lualatex -shell-escape -halt-on-error -interaction=batchmode -jobname "ext-tikz/thesis-figure13" "\def\tikzexternalrealjob{thesis}\input{thesis}"

ext-tikz/thesis-figure14.pdf: 
	lualatex -shell-escape -halt-on-error -interaction=batchmode -jobname "ext-tikz/thesis-figure14" "\def\tikzexternalrealjob{thesis}\input{thesis}"

ext-tikz/thesis-figure15.pdf: 
	lualatex -shell-escape -halt-on-error -interaction=batchmode -jobname "ext-tikz/thesis-figure15" "\def\tikzexternalrealjob{thesis}\input{thesis}"

ext-tikz/thesis-figure16.pdf: 
	lualatex -shell-escape -halt-on-error -interaction=batchmode -jobname "ext-tikz/thesis-figure16" "\def\tikzexternalrealjob{thesis}\input{thesis}"

ext-tikz/thesis-figure17.pdf: 
	lualatex -shell-escape -halt-on-error -interaction=batchmode -jobname "ext-tikz/thesis-figure17" "\def\tikzexternalrealjob{thesis}\input{thesis}"

ext-tikz/thesis-figure18.pdf: 
	lualatex -shell-escape -halt-on-error -interaction=batchmode -jobname "ext-tikz/thesis-figure18" "\def\tikzexternalrealjob{thesis}\input{thesis}"

ext-tikz/thesis-figure19.pdf: 
	lualatex -shell-escape -halt-on-error -interaction=batchmode -jobname "ext-tikz/thesis-figure19" "\def\tikzexternalrealjob{thesis}\input{thesis}"

