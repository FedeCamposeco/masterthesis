\chapter{Methodology} \label{chap:method}
\section{Overview}
In order to achieve proper integration of the existing inertial sensors into a visual pose estimation pipeline, several steps are required as well as the consideration of many different aspects. Having studied the available visual egomotion tracking approaches available, the pattern proposed by Klein and Murray (PTAM) \cite{klein2007parallel} is decided to be used for this research; its compromise of accuracy with performance and the flexibility in its design make it suitable for a mobile application. Moreover, a reference implementation is available online \footnote{\url{http://www.robots.ox.ac.uk/~gk/PTAM/download.html}}, making the task of implementation possible, given the constrained time-frame of this dissertation. 

As its discussed in this chapter, classical visual-inertial fusion algorithms are not suitable for the conditions and available hardware in current smartphones (at least they are not feasible to use in a straightforward way), so the loosely coupled scheme is followed to produce a more reliable and robust solution for such conditions. 

The steps taken for the integration of visual and inertial data are as follow:
\begin{enumerate}
  \item Calculate initial estimate of inertial sensor biases. 
  \item Initialize visual tracker pipeline.
  \item Use an Extended Kalaman Filter to integrate gyro, accelerometer and, when available, visual information to generate a stable orientation estimation updated at every new IMU data arrival.
  \item For every frame to frame motion segment, measure the non-gravitational acceleration projected in the inertial frame. 
  \item Robustly determine a scale from this corresponding visual and accelerometer-based displacements.
  \item Once the scale converges, use its inverse to filter the pose estimated by the visual tracking pipeline. 
  \item Use inertial data to automatically acquire high-resolution images at suitable moments to be incorporated to the dense reconstruction solver. Whenever new frames are added, give audio feedback to the user.
  \item Display on-screen partial reconstruction and allow the user to save the results of the reconstruction. 
\end{enumerate}

The process is split into several threads, thus it is not strictly in this order. To further explain the overview of the proposed system, Figure \ref{fig:final_system} in Section \ref{sec:final_system} depicts the final work-flow; the following sections detail each step of the method.

Although the standard implementation of PTAM was used, several tuning to the parameters was performed to better accommodate the computational capabilities of the smartphone used. A key bottleneck in PTAM is the detection of salient point, done using the FAST detector as described in Section \ref{sec:FAST}. The detector is used in each pixel of every frame and in four different octaves, so the optimization of the FAST algorithm for the smartphone would be of significant impact. This optimization was done using a vectorization engine---better known as Single Instruction Multiple Data (SIMD) engine---available in the Galaxy S3, the NEON general-purpose SIMD engine. 

\section{Feature Detector Vectorization}
Single instruction multiple data, or SIMD, refers to a class of parallel computation approach where multiple processing elements run the same process on different data {\em simultaneously} (Fig \ref{fig:simd}). This is sometimes called ``vectorization'' instruction, since many of the applications of SIMD instructions are in the manipulation of vectors of data. Many modern CPU's include a SIMD co-processor, referred to as the FPU (float-point unit). This co-processor has its own memory registers, instruction set and, sometimes, its own clock-cycles and cache. In the case of Intel's x86 and x64 architectures, the FPU is accessed through a set of Assembly language instructions called the MMX Instruction Set, or by a set of intrinsics that access those instructions through the C programming language.
\graphicspath{{figures/}}
\begin{figure}
  \centering
  \def\svgwidth{200pt}
  \input{figures/simd.pdf_tex}
  \caption{Single Instruction Multiple Data (SIMD) processor.}
  \label{fig:simd}
\end{figure}

In the case of many ARM processors, the available SIMD co-processor is labeled the NEON engine. This architecture runs with 16 128-bit data registers and instructions, enabling the concurrent manipulation of 4-word long data-types, or their fragmented 32 64-bit data types. This makes the architecture very flexible; it supports several data-types, such as unsigned/signed byte, half-word, word, double precision, single precision, etc. As with Intel's instruction set, NEON can be accessed via ARM Assembly or through a set of C language intrinsics.

The performance of the visual SLAM algorithm---PTAM---is bounded by the rate at which the tracker can update the camera pose. In the specific hardware setting used in this research, the camera frame-rate can go up to 30 frames per second, so in order to prevent any frame drops, the tracking thread of must be kept as lean and nimble as possible. Although tuning several parameters to increase the tracker's performance did resulted in the decrease of computational time, a bottleneck for the pipeline is the feature detection process, FAST (for details see Sec. \ref{sec:FAST}). The complete algorithm was re-implemented using the NEON instruction set written in Assembly language. 

\begin{figure}[h!]
  \centering
  \input{figures/neon_fast.pdf_tex}
  \caption{Vectorized implementation of FAST.}
  \label{fig:neon_fast}
\end{figure}

The method used for the parallelization of the detection task was to {\em concurrently} test 10 pixels. This is possible since each NEON register can hold up to 16 bytes, with each byte representing one gray-scale pixel value. All of the pixel intensity tests are performed in the same order as the ones in the sequential algorithm. Instead of comparing each pixel $p_k$ at location $k$ with a 16-pixel ring $S_{k,16}$---first pixel 1 and 4, and so forth to discard non-features rapidly and so forth---a 16 pixel candidate vector $\mathbf{p}_{k \dots k+15}$ is compared to 16 vectors of surrounding pixels $S_{k \dots k+15,16}$ simultaneously, as depicted in Figure \ref{fig:neon_fast}. The result for each concurrent operation is a 10-binary vector mask that specifies whether a pixel in the test vector is a FAST feature or not. The speed-up of the implementation is theoretically of a factor of ten, since for each test-vector of 10, 16 pixels will cover the first and last pixels of the vector. Because of memory limitations and latency between the main and co-processors, the factor was close to only five. 


\section{Inertial Navigation Calibration}  \label{sec:insCalib}
It has been thoroughly discussed in Sections \ref{sec:errChGyr} and \ref{sec:errAcc} how the IMU bias and noise characteristics will affect dramatically the performance of a given INS algorithm.  In order to explore the real capabilities of the available hardware in order to determine the applicability of existing INS algorithms, the noise characteristics of the IMU was experimentally determined. 

\begin{figure}[h!]
  \centering
  \newlength\figureheight 
  \newlength\figurewidth 
  \setlength\figureheight{6cm} 
  \setlength\figurewidth{6cm}
  \input{figures/noiseEval_acc1.tex}
  \caption{Accelerometer noise RMS calculation.}
  \label{fig:noiseEval_acc}
\end{figure}

The operating points for the sensors used were obtained at room temperature and with the device static on the floor. In order to estimate the noise characteristics of the sensors, several seconds of data where gathered whilst the phone was static. The histogram for the output of the sensors was then examined and, assuming a Gaussian distribution, noise characteristics were extracted. RMS values, $\sigma$, are shown with each Gaussian fit shown in Figure \ref{fig:noiseEval_acc}. 

Biases from the accelerometers are more difficult to obtain, since in a resting state the value should not be zero, but will be affected by gravity and the attitude of the body with respect to the inertial frame. The proposed method is to manually and slowly change the attitude of the phone and store the data output from each accelerometer axis. Afterwards a dynamic range can be measured from the maximum and minimum values of each axis. From this test, a bias for each axis can be observed as a shift in the dynamic range of the sensor as shown if Figure \ref{fig:biasEval_acc}. In the case of the gyroscope, gathering data while stationary is enough to determine an estimate of the bias by averaging the obtained values. From data similar to the data shown in Figure \ref{fig:noiseEval_acc}, an average RMS value of $\sigma = 0.0084$ across the 3 axes was determined.

\begin{figure}[h!]
  \centering
  \setlength\figureheight{6cm} 
  \setlength\figurewidth{6cm}
  \input{figures/noiseEval_acc2.tex}
  \caption{Accelerometer bias calculation.}
  \label{fig:biasEval_acc}
\end{figure}

For the purpose of the applicability of a given filtering technique---where the observations of pose come from a  visual tracking algorithm, and the predictions come from the IMU in a loosely coupled integration---it is useful to determine the amount of uncertainty to the pose the inertial predictions would introduce from one frame to the next, in order to determine if the prediction will be indeed useful. Using the IMU's measured noises (see above) coupled with the error dynamics developed in Section \ref{sec:errorsBIMU}, it is possible to get a sense of how the prediction will interact with the visual system updates in case of an unknown visual scale.

Assuming calibrated bias and using the values of $\sigma_g = 0.0198$ and $\sigma_a=0.084$ of gyro and accelerometer respectively and considering an optimistic visual tracker update rate of 1 frame per 30 milliseconds and the nominal 200Hz rate of the IMU, the prediction of the pose from the IMU would have made the position uncertainty grow by the noise in accelerometer {\em and} the noise of gyro of projecting noisily the gravity vector (see Section \ref{sec:errorsBIMU}). First, using Eq. \ref{eq:rms_accel} 
\begin{align}
  \sigma_{s,a}(0.03) = (0.0198)~(0.03)^{1.5}~\sqrt{1/3\cdot 0.005} = 0.0042mm
\end{align}
Which is negligible, however, by using the uncertainty introduced by the gyro error as a tilt error as in Eq. \ref{eq:gyro_error} we get that after the inter-frame period of 30ms, the bound of the tilt error would be 
\begin{align}
  \sigma_{q}= \sqrt{(0.005)~(0.03)}\cdot (0.084) = 0.059^{\circ}
\end{align}
For both roll and pitch directions. Finally, using Eq. \ref{eq:propError1} we get a rough estimate of the uncertainty introduced by the gyroscope noise:
\begin{align}
  \sigma_{s,g}=\frac{1}{2}\cdot (0.03)^2\cdot \left[ \sqrt{2}~9.81(0.0589) ~~ \sqrt{2}~9.81(0.0589) ~~ 0\right]~m=0.52mm
\end{align}
Which, considering that we assume a static scenario, is an important uncertainty. For image features at closer ranges, a motion of the camera of even one millimeter would move the feature by many pixels, so such a high lower-bound error for position from the IMU is something that must be addressed. Furthermore, in reality, the prediction will be worsened by dynamic bias, unmodeled delays in visual pose estimation (sometimes of up to 100ms) and failures in the visual tracker. This is not to say that a classical tightly coupled method will never converge, but the rate of convergence might be impractically slow, since no initial hint of scale is present to predictably mitigate the conditions of the IMU pose prediction. 

In order to alleviate the issues of accuracy introduced by the inertial sensors, as well as the visual tracking failures that might occur, a more robust 2-step outlier-rejection scheme is put in place to {\em first} estimate the scale and biases from significant enough motions and {\em second} filter the output of the inertial and scaled visual outputs. This is detailed in the sequel. 

  \section{Robust Metric Scale Estimation}
  \subsection{Technical Requirements}
In order to effectively fuse the pose estimations from the inertial and visual frameworks, both systems have to agree on the scale in which the visual update will happen. Approaches such as the one in Weiss and Siegwart \cite{weiss_real-time_2012 } require a decent enough initialization of the filter in order to cope with a different scale in the initial visual updates. In the system described herein, the initial vision scale is designed to be unconstrained. This allows for the initialization of PTAM from close distance working environments (\textit{e.g.} a desktop) as well as a far distance environment (\textit{e.g.} a building). 

\begin{figure}[h!]
  \centering
  \setlength\figureheight{5cm} 
  \setlength\figurewidth{8cm}
  \input{figures/vision_outlier.tex}
  \caption{Visual pose estimation outlier compared against ground truth.}
  \label{fig:vision_outlier}
\end{figure}

In order to use the inertial pose estimation to filer the slow-rate pose estimation from PTAM, the scale of the visual pose is calculated in as an initial step. Intuitively, the estimation of scale from the pose estimation, $\hat{s}_i$, from IMU and $\hat{s}_v$ from vision, could simply come from comparing the distance traversed by both estimations and assume the error between those two displacements is the scaling factor: $\hat{s}_i \propto \hat{s}_v$. However, some failures in the visual pipeline due to changes in illumination or motion blur will cause the estimation to fail, as shown in Figure \ref{fig:vision_outlier}. Additionally, the IMU data could also include spurious position estimation errors from either small shakes in the hand of the person holding the phone or uncorrected biases in gyro or accelerometer.  Because of this, the measurements should not be considered to estimate the scale directly, instead a more robust solution is proposed. The main considerations are summed up first:
\begin{itemize}
  \item The visual measurements eventually {\em will} introduce spurious measurements.
  \item Being a hand-held application, small rapid accelerations should be considered hand jitter, since there is not enough signal to noise ratio to give a decent estimation.
  \item In order to conclusively determine that an acceleration signal is not hand jitter, a specific magnitude of motion should be searched for in the acceleration signal.
  \item Hand motion is constrained to a specific set of motions: they occur in small periods of time and the starting and stopping force peaks are similar.
  \item IMU estimations of position in the inertial vertical direction will carry less error. This comes from the effect of tilt errors on the projection of the gravity vector. For an exhaustive discussion on this, see Section \ref{sec:errorsBIMU}.
\end{itemize}
This operating conditions call for a robust system that:
\begin{itemize}
  \item Operates with motion \textbf{events}. Events are sections of displacement determined by {\em sufficiently exciting motion}. This automatically rejects small jerky hand motions. Each event has a data pair of IMU and vision displacements.
  \item Is capable of keep track of a large number of measured event pairs and incrementally reject spurious visual estimations. 
  \item Lastly, it should give a higher level of confidence based on the displacement in the inertial-frame vertical direction estimated by the IMU.
  \item Employs a decaying velocity model to mitigate small biases in the IMU signal.
\end{itemize}
An immediate drawback from this system is that in order for it to gather the needed data, the user should move the phone in a specific way: relatively fast and short segments of motion with little change in attitude throughout. Although this seems like an excessive requirement for the user, the required motion is actually very natural if the user is trying to get a diverse set of view-angles for his realtime dense reconstruction. 

  \subsection{Decaying Velocity Model}
A second disadvantage is the need for a decaying velocity model: in order to converge to the true scale value, more samples would be needed to account for the modelling error introduced as an arbitrary decay in speed. In case of motion events of sufficient time, the position estimation would drift too much if even a small bias is present in the acceleration signal.

The decaying velocity model introduces an tunable parameter $\alpha$ with a value close to one to limit the contribution of an overestimated velocity to the position. Once the acceleration is small enough, it is assumed that velocity should approach zero (geometrically decay) since constant velocity is not possible for long distances in a hand-held scenarios. The integration scheme is then expressed by:

\begin{align}
  s_t=s_{t-1}+\alpha\cdot v_t T + \frac{1}{2}a_tT^2
\end{align}

A comparison against the usual velocity model can be seen from Figure \ref{fig:accel_drift}. Here, small errors in bias are amplified to bigger errors of velocity as a velocity bias and they finally end up making position drift indefinitely.

\begin{figure}[h!]
  \centering
  \setlength\figureheight{5cm} 
  \setlength\figurewidth{10cm}
  \input{figures/accel_drift.tex}
  \caption{Comparison of decaying velocity and non-decaying velocity effect on drift in position.}
  \label{fig:accel_drift}
\end{figure}

\subsection{Proposed Method} \label{sec:proposed_method}

  The proposed solution for building a scale estimator as described above, has as a first step to effectively {\em segment} the accelerometer signal based on a common shape of signal of the sought motion (fast short motions). Such signal exhibits the characteristics shown in Figure \ref{fig:accel_events}. Implemented as a Finite State Machine (FSM), the developed system will first detect the pattern of an inertial motion event; which is signaled by two opposite sign, similar magnitude peaks in a relatively short period of time. 

\begin{figure}[h!]
  \centering
  \setlength\figureheight{4cm} 
  \setlength\figurewidth{10cm}
  \input{figures/accel_events.tex}
  \caption{Acceleration signal of sufficiently exciting motion in the vertical direction.}
  \label{fig:accel_events}
\end{figure}

When such event is registered, the decaying velocity model is used to integrate the accelerometer segmented signal into a displacement. This displacement, $\hat{s}_i$, is then stored as the $k^{th}$ measurement pair along with the visual displacement counterpart in a {\em displacement pair queue}: $\{\hat{s}_i,\,\hat{s}_v\}^k$. The queue has a limited number of $N$ elements, in order to keep the outlier rejection process tractable. With each new entry to the queue $\mathcal{Q}$, a new candidate scale is computed from the newest pair alone. This candidate scale is checked against all the measurements in the queue. If the new scale has more inliers than the current best scale, a new scale is computed from the new inlier set $M$ that minimizes 
\newcommand{\argmin}{\operatornamewithlimits{argmin}}

\begin{align}
  \argmin_\lambda\sum_m^M(\hat{s}^m_i-\lambda\cdot\hat{s}^m_v)^2 \label{eq:lls_scale}
\end{align}

 If the new candidate scale does not produce more inliers than the current best estimate of $\lambda$, then the new measurement pair is kept in the queue and the old scale is checked against the candidate scale. If the new pair agrees with the old scale the inlier count is increased and the old scale adjusted using Eq. \ref{eq:lls_scale}. The method is summarized in Algorithm \ref{algo:scale}. 

 This scheme will, at every new observation, maintain an outlier-free LLS estimation of the scale. It depends on the input of the displacement of vision and IMU to be close to real values, but the set of measurement now supports a portion of outliers. With each new measurements. The linear solver will also return an error value which, along with the number of inliers, $\left|\mathcal{Q}_i\right|$ is used to heuristically determined the scale as either converged, or not yet converged. After it is said to converge, the scale estimator continues to run in order to continually refine the scale estimation. \

 \begin{Algorithm}[h!]{11cm}
   \SetKwComment{tpy}{\#}{}
   \SetAlgoLined
   \DontPrintSemicolon
   \SetKwFunction{LLS}{LLS\_Solve}
   \KwIn{The new pair $\{\hat{s}_i,\,\hat{s}_v\}^k$} 
   \KwData{Current best scale $\lambda^*$. Queue $\mathcal{Q}$. Set of inliers $\mathcal{Q}_i \subseteq \mathcal{Q}$}
   \BlankLine
   $\mathcal{Q}\gets\{\hat{s}_i,\,\hat{s}_v\}^k\cup\mathcal{Q}$\;
   $\lambda_c\gets\hat{s}^k_i/\hat{s}^k_v$\;
   $\mathcal{Q}_{i2}\gets\varnothing$\;
   \BlankLine
   \ForEach{$\{\hat{s}_i,\,\hat{s}_v\}^j\in \mathcal{Q}$}{
     \BlankLine
     \If{$ | \lambda_c\cdot\hat{s}^j_v - \hat{s}^j_i| < \epsilon $}{
       $\mathcal{Q}_{i2}\gets\{\hat{s}_i,\,\hat{s}_v\}^j\cup\mathcal{Q}_{i2}$\;
     }
   }
   \BlankLine
   \eIf{$\left|\mathcal{Q}_{i2}\right|>\left|\mathcal{Q}_{i}\right|$}{
     $\mathcal{Q}_i\gets\mathcal{Q}_{i2}$\;
     $\lambda^*\gets$ \LLS{$\mathcal{Q}_i$} \tpy*[f]{\LLS{$\cdot$} using Eq.$\,$\ref{eq:lls_scale}~}
   }{
     $\lambda^*\gets$ \LLS{$\mathcal{Q}_i$}\;
   }
   \BlankLine
   \KwResult{Updated scale $\lambda^*$ and set of inliers $\mathcal{Q}_{i} \subseteq \mathcal{Q}$}
   \caption{Robust Scale Estimator}
   \label{algo:scale}
 \end{Algorithm}

 
 \section{Visual-Inertial Filtering}\label{sec:vis_imu_filt}
 \subsection{Zero Motion Update}
 Although the initial biases are assumed to be determined prior to the execution of the algorithm, the values are constantly refined by the exploitation of the visual data available. Whenever there exist a reliable measurement from the visual pipeline that indicates that the motion of the body is close to zero, a so-called Zero Motion Update (ZUPT) takes place. The ZUPT will usually help to limit the uncertainty in a usual INS, however for the method described here, a more direct method of using the ZUPT is possible. 

 At each indication of no-motion, the speed of the device is assumed to be almost zero, correcting the initial velocity estimation for the next IMU pose estimation prediction step. Additionally, the biases of the gyroscope can be improved, since any value far from zero in the de-biased input from the accelerometer is considered as an uncorrected bias. Because of noise considerations, the bias estimation is not replaced, but filtered using weighted averages, where the weight depends on the vision-based velocity estimated, as follows
 \begin{align}
   \vec{b}^{~k+1}_g=\frac{\beta \left|\vec{v}^{~k}_v\right| \vec{\omega}^{~k}_g + \vec{b}^{~k}_g}{\beta \left|\vec{v}^{~k}_v\right|+1}
 \end{align}

 Where $\left|\vec{v}^{~k}_v\right|$ is the magnitude of the estimated velocity by vision, $\vec{\omega}^{~k}_g$ is the de-biased gyro measurements and $\vec{\omega}^{~k}_g$ is the current bias estimation.

\subsection{Timing Considerations}\label{sec:timing_considerations}
Special consideration must be given to the timing of the ZUPT in order to validate the accuracy of the update. Since there is no guarantee that the time reported by the Android's API corresponds to the exact time of image acquisition nor is it consistent with the IMU timer, the delay between the visual information and the IMU information must be empirically determined. Also, for the actual filtering of the pose that will result as output when the scale converges, the relative timing of the IMU and vision poses is essential.

\begin{figure}[h!]
  \centering
  \setlength\figureheight{6cm} 
  \setlength\figurewidth{10cm}
  \input{figures/timing_imuvis.tex}
  \caption{Experimental visual-inertial delay calculation.}
  \label{fig:imuvis_time}
\end{figure}


The method to experimentally determine the vision to IMU delay is to submit both estimation pipelines to pure rotation. Afterwards, the IMU orientation output signal (explained in the sequel) in a given axis and the rotation of the same axis from the visual estimation---as shown in Figure \ref{fig:imuvis_time}---are compared. It is assumed then that the difference in the aligned signals is due to a phase difference between the orientation estimation. This phase, labeled $\phi_{v,i}$, is determined in a linear least square sense from a sample of the data points.

\subsection{Orientation Estimation}
Visual-inertial fusion, as well as the robust scale estimation, require that a relatively accurate orientation estimation is available. As opposed to inertial position, orientation estimation can be relatively successful, even using consumer-end hardware. From gravitational and rotation rate information it is possible to fully determine the roll and pitch angles in the North East Down (NED) inertial coordinate system. 

\begin{figure}[h!]
  \centering
  \input{figures/ned_frame.pdf_tex}
  \caption{North East Down and Earth Centered Fixed frames.}
  \label{fig:ned_frame}
\end{figure}

In a NED coordinate system, the $x$-axis is pointing East, the $y$-axis is pointing North and (preserving the right-hand rule) the $z$-axis is pointing down collinear with the Earth's gravity force vector; the origin of the system is chosen to be the center of mass of the body (see Figure \ref{fig:ned_frame}). Thus, the only absolute NED oriented information come from the accelerometer, since from the projection of the body-measured gravity vector we can establish the rotations around $x$ and $y$ in the NED frame. This information can then be refined further by the use of a gyroscope, since the gyro is more sensitive and has a better dynamic response. The yaw angle, however, cannot be determined from accelerometer-gyro alone, and the use of a magnetometer (or a compass) is needed. In outdoor applications, the integration of a magnetometer is standard, however, for electromagnetically polluted environments (such as indoors or cities) magnetometers are unreliable because of interference from that pollution. 

The goal of the orientation estimation is then to establish a frame where the $z$-axis is aligned with the gravity vector but the  $x$ and $y$-axes are free. A filtering scheme from integrating the gyroscope information with the accelerometer's, would bring consistent updates for the roll and pitch angles, but the yaw rotation would basically be subject to unbounded drift. Because of this, it is intended that the orientation estimation method handles this drift by means of the visual information, whenever available. Visual yaw angle information, $m_B$ is then used to produce the final body to inertial rotation $R_B$. Although the vector $m_B$ is not oriented with the magnetic north, its (albeit arbitrary) value will not drift over time for a single experiment. The gravity vector $g_B$ is normalized to the unit-length vector $z_B$ and it is just left to estimate $y_B$ and $z_B$ using the additional heading data.

\begin{align}
  r_{zB} = \frac{g_{B}}{\|g_{B}\|}, \hspace{7pt} r_{yB} = \frac{r_{zB} \times m_{B}}{\|r_{zB} \times m_{B}\|},\hspace{7pt} 
  r_{xB} = r_{yB} \times r_{zB},  \hspace{15pt} 
\end{align}

resulting in the projection matrix $R_{B}$ as

\begin{align}
  R_B=[r_{xB},r_{yB},r_{zB}]\ \in \mathcal{SO}(3) %\hspace{7pt} \dot{R}_B=\vec{\omega}R,
\end{align}
Where  $\mathcal{SO}(3)$ is the space of 3D rotations. Since we have small rotation increments from $t$ to $t+1$, we use Eq. \ref{eq:quat_deriv} to get the prediction equation of the orientation as

\begin{align}
  \hat{R}_B&=e^{\vec{\omega}dt}\bar{R}^-_B,\\
  \bar{r}_{iB}^+&=\hat{r}_{iB}+L_{ik}(z_i-\hat{r}_{iB}) \ \text{with}\ i\in (x,y,z).
\end{align}

where the Kalman gain matrix $L_k$ is computed in every time step with the linearized system. The predictions then come from the gyroscope, $\vec{\omega}$, and the updates come from the combination of the visual and gravity vector observations. This requires also timing considerations discussed in Section \ref{sec:timing_considerations}.

\subsection{Position Filtering}
One final missing step is the actual filtering of the IMU and Vision data for position estimation. In a consistent orientation scenario, as achieved by the Orientation Estimation, the only thing left to do is to wait for the scale to converge and then use both position signals to produce a better one. At this point of the process, the gyroscope should be de-biased completely, and the scale between systems should be consistent as well. 

For each new IMU measurement, the acceleration is projected to the local inertial frame, double integrated using a decaying velocity model and the output is given as the pose of the complete system. With each new vision position datum, the position given is first scaled to metric units and then the state of the integrator is updated using a Direct Filter, shown to exponentially converge given de-biased inertial values in \cite{baldwin_complementary_2007}:
\begin{align}
  \hat{v}^{k+1}_i &= R_B\vec{v}^{~k}_i+K_p R^t_B (\hat{s}^{~k}_i - \vec{s}^{~k}_i) \\
  \hat{s}^{k+1}_i &= \tau \hat{v}^{~k+1}_i + \hat{s}^{k}_i 
\end{align}

Where $K_p$ is the tunable parameter of the filter, but it should lie somewhere between zero and Nyquist frequency of the sampling rate of the pose estimation \cite{baldwin_complementary_2007}.

\section{Complete System} \label{sec:final_system}

\begin{figure}[h!]
  \centering
  \scalebox{.88}
  {
    \input{figures/dia/full_system_2}
  }
  \caption{Complete system workflow.}
  \label{fig:final_system}
\end{figure}

