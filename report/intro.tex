\chapter{Introduction} \label{chap:intro}

In biological vision, Structure from Motion (SfM) refers to the phenomenon by which humans (as well as other animals) can extract 3D information from 2D retinal {\em images} of a moving scene. It is a long-standing area of interest in Computer Vision to try to emulate such phenomenon using a single camera in motion. While humans are equipped with proprioception and equilibrioception that provide information about the movement of the eyes with respect to the world (also known as kinesthesis), a digital camera does not \cite{corke_introduction_2007}. Therefore, a natural approach should be the inclusion of inertial measurement units (IMUs) that can act as such proprioceptors in humans. 

In Computer Vision, solving the SfM problem amounts to extracting a 3D geometry from a set of 2D images, as well as the poses of the camera at the moment of that such images were produced. Thus, it can be said that solving the SfM problem means solving the Simultaneous Localization and Mapping problem (SLAM), a paramount step for autonomous robotics. As such, the terms are used interchangeably in this work. While it is understood by some that SfM differs to SLAM in that it is offline; the usage here does not make suchlike restriction. 

It is common knowledge in the field of Computer Vision \cite{Hartley2000} that it is not possible to infer the metric scale from a monocular geometric information alone. Addressing this issue, this document will first discuss the different approaches to realtime SfM and will then use this as a background to choose a concrete framework suited for the application (see Section \ref{sec:realtime_sfm}). Using this framework, {\em a novel method of using the input data from the on-board IMU in a robust scheme will be exploited in virtue of a smoothened metric pose estimation.}

An important real-world motivation of this work is to enable a smartphone end-user, to {\em interactively} acquire a 3D dense reconstruction of his surroundings, in realtime and in a metric scale. There exist some solutions in the market that allow an end-user such reconstruction from a monocular setup, however as it will be discussed in Section \ref{sec:intro:exist_sols}, these systems suffer from several problems. Specifically, the reconstruction is not guaranteed to succeed, as images taken for the reconstruction might be ill-posed or simply insufficient. This is commonplace to someone familiar with SfM, since the correct solution of the 3D point-cloud depends on sufficient and well-suited image data. Given that the problem of obtaining the correct dataset is not trivial (even for an expert), {\em a new method to automatically acquire image data for the concurrent 3D reconstruction pipeline is also herein described.}    

\section{Problem Definition} \label{sect:intro:problem}
Given a video feed and inertial measurements from a gyroscope and an accelerometer on a smartphone, a 3D mapping of a scene  and the relative pose of the phone with respect to the map must be delivered in realtime and in metric scale. Additionally, the system must leave enough computational power unused to enable a dense reconstruction of an object to be built using the pose information in relatime on board the phone (for additional details see Tanskanen \textit{et al.} \cite{Tankanen_3dlive_2013}). 

A method to aid the user in the acquisition of suitable images for the purpose of the dense reconstruction needs to be set in place as well. The partial results of the reconstruction must be displayed on-screen so that the user can visually asses the quality or completeness of the reconstruction, so as to be able to fill-in sparse areas and detect failed attempts. An overview of the work-flow is shown in Figure \ref{fig:workflow}, the scope of the contributions for this dissertation are delimited inside the dashed-line box. 

\begin{figure}[h!]
  \centering
  \scalebox{0.93}
  {
    \input{figures/dia/overview}
  }
  \caption{Overview of proposed pipeline.}
  \label{fig:workflow}
\end{figure}

\subsection{Existing User-End Solutions}\label{sec:intro:exist_sols}
While nowadays there are consumer-level solutions available that deal with monocular 3D reconstruction, they are indeed limited in several aspects and none are available to the mobile market. For example, two popular solutions in the market are 123D Catch by Autodesk and Vi3Dim by Vi3Dim Technologies. 

In the case of Vi3Dim, although there is no readily available documentation, in an online video \footnote{\url{http://www.youtube.com/watch?v=Vs6IzPCtABg}} it is stated that the system specifically requires that the images used for the dataset are taken from a tripod using an object on a turntable as a reconstruction target. Furthermore, the processing time of the pipeline is rather high (abut 30 minutes for a dataset of less than 350 images). The systems operates with video data and will produce a dense textured 3D reconstruction of the object over the turntable.  
In the case of 123D from Autodesk \footnote{\url{http://www.123dapp.com/}}, it will work with a set of images. The user must capture a set of 20 to 40 images and upload them to Autodesk`'s online reconstruction service. 

With these two systems, as well as with all other similar solutions of the sort available, the user must ``blindly'' capture the data in order to then go through a relatively long offline process to find the results of the reconstruction using their captured data. In many cases, the reconstruction will fail, as the state-of-the-art algorithms are not yet robust enough to handle all of the possible situations and conditions of operation that an end-user might try. Furthermore, if the reconstruction fails, in many cases the user will no longer have the object he wishes to reconstruct available to him (\textit{e.g.} the user wants to reconstruct a museum piece), since the reconstruction is done offline. 

Additionally, all reconstructions done using a monocular camera only, will always lack a metric scale in its reconstructed model, since there is no way to infer size from the scene geometry alone. This burdens the applicability of such reconstructions to scenarios where a specific size is needed. 

Attempting to equip a smartphone with a system such that it can extract a 3D metric reconstruction of the scene and estimating the metric scake using an IMU in realtime would tackle the limitations described above, nevertheless, this is a challenging task. Compared with the hardware used for existing solutions (full-fledged desktop personal computers), even the newest smartphones to date are tremendously underpowered. Also in a mobile solution, the acquisition of the image data relies on the user holding the phone in-hand (as restricting the acquisition procedure to a tripod would defeat the purpose of the system). Hand-held motion is highly erratic and unconstrained, thus it is harder to estimate, and the IMU's applicability for such estimation is more difficult, as it will be discussed in Section \ref{sect:intro:fusion}. 

The system described herein to address the aforementioned issues is based on an eclectic method to leverage visual pose information provided by Visual Tracking with IMU-based pose data. As mentioned, on top of this a dense reconstruction pipeline is used to finally extract, interactively, dense 3D information from a real-world scene. Several complications arise on both the visual and the inertial sides of the solution. In this introduction, the limitations of IMU-generated pose data are addressed (in section \ref{sect:intro:fusion}) as well as the different options for visual pose estimation and their characteristics are discussed (in the sequel).

